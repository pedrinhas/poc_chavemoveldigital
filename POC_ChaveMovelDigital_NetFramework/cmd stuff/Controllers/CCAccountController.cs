﻿using PortalBase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalBase.Controllers
{
    public class CCAccountController :Controller
    {
        public ActionResult SimulationAuth()
        {
            _auth.SimulateLogin();

            return View("Index", "Home");
        }


    }
}