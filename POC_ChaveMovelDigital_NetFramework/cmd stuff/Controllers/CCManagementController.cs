﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using System.Data;
using System.Configuration;

using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;
using AuthRequest = PortalBase.Models.Saml.AuthRequest;

using PortalBase.Models;
using ServiceProviderSample.SAML;
using System.IO;
using System.Text;



using System.Collections.Specialized;
using System.Web.Hosting;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.ComponentModel;
using ServiceProviderSample.SAML;
using System.Net;

namespace PortalBase.Controllers
{
    public class CCManagementController :  BaseController
    {

        private static readonly XmlSerializerNamespaces xmlNamespaces = new XmlSerializerNamespaces();
        NameValueCollection appSettings = ConfigurationManager.AppSettings;
        string relayStateToBepersistedAcross = string.Empty;


        // GET: CCManagement
        public ActionResult Index()
        {

            //AccountSettings accountSettings = new AccountSettings();
            //AuthRequest req = new AuthRequest(new AppSettings(), accountSettings);
            //Response.Redirect(accountSettings.idp_sso_target_url + "?SAMLRequest=" + Server.UrlEncode(req.GetRequest(AuthRequest.AuthRequestFormat.Base64)));


            return View();
        }


        public ActionResult Consume()
        {
            // replace with an instance of the users account.
            AccountSettings accountSettings = new AccountSettings();

            Saml.Response samlResponse = new Saml.Response(accountSettings);
            samlResponse.LoadXmlFromBase64(Request.Form["SAMLResponse"]);

            if (samlResponse.IsValid())
            {
                Response.Write("OK!");
                Response.Write(samlResponse.GetNameID());
            }
            else
            {
                Response.Write("Failed");
            }
            return View("Index");
        }


        public ActionResult Send()
        {
            //if (Request.UrlReferrer.ToString().Contains("?ReturnUrl="))
            //{
            //    int RELAY_STATE_MAX_LENGHT = 80;
            //    relayStateToBepersistedAcross = Request.UrlReferrer.ToString();
            //    relayStateToBepersistedAcross = relayStateToBepersistedAcross.Substring(relayStateToBepersistedAcross.IndexOf("?ReturnUrl="));
            //    relayStateToBepersistedAcross = System.Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(relayStateToBepersistedAcross));
            //    if (relayStateToBepersistedAcross.Length > RELAY_STATE_MAX_LENGHT) relayStateToBepersistedAcross = string.Empty;
            //}

            // GERAÇÃO AUTOMÁTICA DE CÓDIGO DAS CLASSES A PARTIR DAS SCHEMAS, USANDO A FERRAMENTA XSD.exe
            // A definição da classe AuthnRequestType foi obtida automaticamente a partir da schema saml-schema-protocol-2.0.xsd
            // disponível em http://docs.oasis-open.org/security/saml/v2.0/saml-schema-protocol-2.0.xsd a partir da 
            // ferramenta XSD.exe (http://msdn.microsoft.com/en-us/library/x6c1kb0s(v=VS.90).aspx).
            // Para o exemplo corrente foi usada a consola do Visual Studio, corendo-se o comando
            // > xsd.exe saml-schema-assertion-2.0.xsd saml-schema-protocol-2.0.xsd xenc-schema.xsd xmldsig-core-schema.xsd /c /eld
            // A execução deste comando gera um ficheiro com o nome saml-schema-assertion-2_0_saml-schema-protocol-2_0_xenc-schema_xmldsig-core-schema.cs
            // cujo conteúdo foi colocado no ficheiro AuthnRequest.cs incluido no projecto corrente.
            AuthnRequestType _request = new AuthnRequestType();

            // saml-core-2.0-os - 3.2.1 
            // An identifier for the request. It is of type xs:ID and MUST follow the requirements specified in Section
            // 1.3.4 for identifier uniqueness. The values of the ID attribute in a request and the InResponseTo
            // attribute in the corresponding response MUST match
            _request.ID = "_" + Guid.NewGuid().ToString();

            // saml-core-2.0-os - 3.2.1 
            // The version of this request. The identifier for the version of SAML defined in this specification is "2.0".
            // SAML versioning is discussed in Section 4.
            _request.Version = ConfigurationManager.AppSettings["SAML.Version"];

            // saml-core-2.0-os - 3.2.1 
            // The time instant of issue of the request. The time value is encoded in UTC, as described in Section
            // 1.3.3.
            _request.IssueInstant = DateTime.UtcNow;

            // saml-core-2.0-os - 3.2.1 
            // A URI reference indicating the address to which this request has been sent. This is useful to prevent
            // malicious forwarding of requests to unintended recipients, a protection that is required by some
            // protocol bindings. If it is present, the actual recipient MUST check that the URI reference identifies the
            // location at which the message was received. If it does not, the request MUST be discarded. Some
            // protocol bindings may require the use of this attribute (see [SAMLBind]).

            //_request.Destination = "https://autenticacao.cartaodecidadao.pt/Default.aspx";
            _request.Destination = ConfigurationManager.AppSettings["SAML.Destination"];

            // saml-core-2.0-os - 3.2.1 
            // Indicates whether or not (and under what conditions) consent has been obtained from a principal in
            // the sending of this request. See Section 8.4 for some URI references that MAY be used as the value
            // of the Consent attribute and their associated descriptions. If no Consent value is provided, the
            // identifier urn:oasis:names:tc:SAML:2.0:consent:unspecified (see Section 8.4.1) is in
            // effect.
            _request.Consent = ConfigurationManager.AppSettings["SAML.Consent"];

            // saml-core-2.0-os - 3.4.1 
            // A URI reference that identifies a SAML protocol binding to be used when returning the <Response>
            // message. See [SAMLBind] for more information about protocol bindings and URI references defined
            // for them. This attribute is mutually exclusive with the AssertionConsumerServiceIndex attribute
            // and is typically accompanied by the AssertionConsumerServiceURL attribute.
            _request.ProtocolBinding = ConfigurationManager.AppSettings["SAML.ProtocolBinding"];

            // saml-core-2.0-os - 3.4.1 
            // Specifies by value the location to which the <Response> message MUST be returned to the
            // requester. The responder MUST ensure by some means that the value specified is in fact associated
            // with the requester. [SAMLMeta] provides one possible mechanism; signing the enclosing
            // <AuthnRequest> message is another. This attribute is mutually exclusive with the
            // AssertionConsumerServiceIndex attribute and is typically accompanied by the
            // ProtocolBinding attribute.
            _request.AssertionConsumerServiceURL = ConfigurationManager.AppSettings["SAML.AssertionConsumerServiceURL"];

            // saml-core-2.0-os - 3.4.1 
            // Specifies the human-readable name of the requester for use by the presenter's user agent or the
            // identity provider.
            _request.ProviderName = ConfigurationManager.AppSettings["SAML.ProviderName"];

            // saml-core-2.0-os - 2.2.5
            // The <Issuer> element, with complex type NameIDType, provides information about the issuer of a
            // SAML assertion or protocol message. The element requires the use of a string to carry the issuer's name,
            // but permits various pieces of descriptive data (see Section 2.2.2).
            _request.Issuer = new NameIDType();

            // TODO: criar novo service provider em ambiente de desenvolvimento/teste a criar e disponibilizar na
            // internet, assim como um certificado específico para este exemplo neste ambiente:
            _request.Issuer.Value = ConfigurationManager.AppSettings["SAML.Issuer.Value"];

            // saml-core-2.0-os - 3.2.1
            // This extension point contains optional protocol message extension elements that are agreed on
            // between the communicating parties. No extension schema is required in order to make use of this
            // extension point, and even if one is provided, the lax validation setting does not impose a requirement
            // for the extension to be valid. SAML extension elements MUST be namespace-qualified in a non-
            // SAML-defined namespace.
            _request.Extensions = new ExtensionsType();

            XmlDocument docAux = new XmlDocument();
            docAux.PreserveWhitespace = true;

            // Elemento RequestedAttributes
            XmlElement requestedAttributes = docAux.CreateElement("fa", "RequestedAttributes", "http://autenticacao.cartaodecidadao.pt/atributos");

            #region Atributos disponíveis do Cidadão

            requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/NIC", true));
            requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/NIF", true));
            requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/NomeCompleto", true));
            //requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/DataNascimento", true));
            //requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/Nacionalidade", true));
            //requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/NICCifrado", false));
            //requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/NIFCifrado", false));
            //requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/NISS", false));
            //requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/NISSCifrado", false));
            //requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/NomeApelido ", true));
            //requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/NomeProprio", true));
            //requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/Cidadao/NumeroSerie", true));
            //requestedAttributes.AppendChild(buildRequestedAttribute(docAux, "http://interop.gov.pt/MDC/FA/PassarConsentimento", true));
            
            #endregion


            //Chave Móvel Digital
            XmlElement cmdAttr = docAux.CreateElement("fa", "FAAALevel", "http://autenticacao.cartaodecidadao.pt/atributos");
            cmdAttr.InnerText = ConfigurationManager.AppSettings["SAML.FAAALevel"];


            _request.Extensions.Any = new XmlElement[] { requestedAttributes, cmdAttr };

            XmlDocument doc = null;

            // Converter objeto para XmlDocument via stream usando serialização com os tipos AuthnRequestType e XmlDocument
            // http://support.microsoft.com/kb/815813/en-us
            try
            {
                MemoryStream stream = new MemoryStream();
                XmlSerializer requestSerializer = new XmlSerializer(_request.GetType());
                requestSerializer.Serialize(stream, _request, xmlNamespaces);
                stream.Flush();

                StreamReader reader = new StreamReader(stream);
                stream.Seek(0, SeekOrigin.Begin);
                XmlTextReader xmlReader = new XmlTextReader(new StringReader(reader.ReadToEnd()));

                XmlSerializer xmlDocumentSerializer = new XmlSerializer(typeof(XmlDocument));
                doc = (XmlDocument)xmlDocumentSerializer.Deserialize(xmlReader);
                doc.PreserveWhitespace = true;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao converter objecto para XmlDocument: " + ex.ToString());
            }

            // assinatura:

            // Obter certificado de ficheiro
            //string SamlCertificateName = appSettings.Get("SamlCertificateName");
            //string SamlCertificatePassword = appSettings.Get("SamlCertificatePassword");
            X509Certificate2 cert = new X509Certificate2(HostingEnvironment.MapPath("~/SAML/Certificate/") + "chave_privada_certificado_e_cadeia.pfx", "");
            //X509Certificate2 cert = new X509Certificate2(HostingEnvironment.MapPath("~/SAML/Certificate/") + appSettings.Get("SamlCertificateName"), appSettings.Get("SamlCertificatePassword"));
            RSACryptoServiceProvider rsaCsp = (RSACryptoServiceProvider)cert.PrivateKey;

            #region exemplo de uso de certificado instalado na keystorage "Personal" da conta "Computer"
            //// thumbprint do certificado a usar na assinatura - aplicação tem que correr com permissões de administração
            //// TODO: criar novo service provider "sampleSP" em testes e criar um certificado para este fim:

            //string thumbprint = appSettings.Get("CertThumbprint");
            ////string thumbprint = "CertThumbprint";

            ////// Exemplo de obter certificado da keystore: http://support.microsoft.com/default.aspx?scid=kb;en-us;901183
            ////// Outro método: http://icodesnip.com/snippet/csharp/getting-x509-certificates-in-and-out   -of-the-key-store

            //X509Store store = null;
            //X509Certificate2 cert = null;
            //try
            //{
            //    thumbprint = thumbprint.ToUpper();

            //    //Exemplo adaptado de http://msdn.microsoft.com/en-us/library/system.security.cryptography.x509certificates.x509certificate2collection.find.aspx
            //    store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            //    store.Open(OpenFlags.ReadOnly);
            //    X509Certificate2Collection certCollection = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);
            //    cert = certCollection[0];
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Excepção ao pesquisar certificado: " + ex.ToString());
            //}
            //finally
            //{
            //    if (store != null)
            //        store.Close();
            //}


            //// Quando se utiliza um certificado instalado na keystore do windows
            //// é necessário dar privilégios de acesso à chave privada de assinatura do certificado ao utilizador
            //// que corre a aplicação - "NETWORK SERVICE" e "ASPNET" em Windows Server 2003, utilizando a ferramenta
            //// winhttpcertcfg (http://www.microsoft.com/download/en/details.aspx?displaylang=en&id=19801),
            //// executando os comandos 
            //// > winhttpcertcfg -g -c LOCAL_MACHINE\My -s "<certificate subjet name>" -a "NETWORK SERVICE"
            //// > winhttpcertcfg -g -c LOCAL_MACHINE\My -s "<certificate subjet name>" -a "ASPNET"
            //// ou, no IIS 7.5 dando permissões ao utilizador da application pool que corre o web site, 
            //// e.g. "IIS AppPool\DefaultAppPool", a partir de uma consola MMC, em "mais acções->Todas as Tarefas->Gerir Chaves Privadas"
            #endregion

            // Código adaptado do exemplo em http://msdn.microsoft.com/en-us/library/k0zd758e.aspx
            try
            {
                XmlElement element = doc.DocumentElement;
                SignedXml signedXml = new SignedXml(element);
                signedXml.SigningKey = cert.PrivateKey;

                // Tipo de dados "ID" é restrito às strings em NCName:
                //<xs:simpleType name="ID" id="ID">
                //  <xs:annotation>
                //    <xs:documentation source="http://www.w3.org/TR/xmlschema-2/#ID"/>
                //  </xs:annotation>
                //  <xs:restriction base="xs:NCName"/>
                //</xs:simpleType>
                // NCName está definido em http://www.w3.org/TR/1999/REC-xml-names-19990114/#NT-NCName como:
                // NCName	 ::=	(Letter | '_') (NCNameChar)*
                Reference reference = new Reference("#" + element.Attributes["ID"].Value);

                // Vide 5.4.3 "Canonicalization Method" e 5.4.4 "Transforms" em 
                // http://docs.oasis-open.org/security/saml/v2.0/saml-core-2.0-os.pdf
                reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
                reference.AddTransform(new XmlDsigExcC14NTransform());

                signedXml.AddReference(reference);
                signedXml.KeyInfo.AddClause(new KeyInfoX509Data(cert));
                signedXml.ComputeSignature();
                XmlElement xmlDigitalSignature = signedXml.GetXml();

                //AuthnRequestType define a ordem dos elementos filhos na schema saml-schema - protocol - 2.0.xsd:
                //<complexType name="RequestAbstractType" abstract="true">
                //    <sequence>
                //        <element ref="saml:Issuer" minOccurs="0"/>
                //        <element ref="ds:Signature" minOccurs="0"/>          
                //        <element ref="samlp:Extensions" minOccurs="0"/>
                //    </sequence>
                //    ...
                //</complexType>
                XmlNode refNode = doc.GetElementsByTagName("Issuer", ConfigurationManager.AppSettings["SAML.Issuer"]).Item(0);
                element.InsertAfter(xmlDigitalSignature, refNode);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepção ao assinar Xml:" + ex.ToString());
            }

            // Vide 3.5.3 "RelayState" em 
            // http://docs.oasis-open.org/security/saml/v2.0/saml-bindings-2.0-os.pdf
            // "...The value MUST NOT exceed 80 bytes in length and SHOULD be integrity protected by the entity 
            // creating the message independent of any other protections that may or may not exist during message 
            // transmission..."
            //this.RelayState.Value = relayStateToBepersistedAcross;
            string RelayState = relayStateToBepersistedAcross;

            // Vide 3.5 "HTTP POST Binding" em 
            // http://docs.oasis-open.org/security/saml/v2.0/saml-bindings-2.0-os.pdf
            // "The HTTP POST binding defines a mechanism by which SAML protocol messages may be transmitted
            // within the base64-encoded content of an HTML form control."
            //this.SAMLRequest.Value = Convert.ToBase64String(Encoding.UTF8.GetBytes(doc.OuterXml));
            string SAMLRequest = Convert.ToBase64String(Encoding.UTF8.GetBytes(doc.OuterXml));

            string Action = ConfigurationManager.AppSettings["SAML.Action"];

            //Response.Redirect(Action + "?SAMLRequest=" + SAMLRequest);

            AuthSAMLRequestModel item = new AuthSAMLRequestModel();
            item.Action = Action;
            item.RelayState = RelayState;
            item.SAMLRequest = SAMLRequest;

            return View(item);
        }


        private XmlElement buildRequestedAttribute(XmlDocument xmlDoc, string attributeName, bool isRequired)
        {
            XmlElement requestedAttr = xmlDoc.CreateElement("fa", "RequestedAttribute", "http://autenticacao.cartaodecidadao.pt/atributos");
            requestedAttr.SetAttribute("Name", attributeName);
            requestedAttr.SetAttribute("NameFormat", ConfigurationManager.AppSettings["SAML.NameFormat"]);
            requestedAttr.SetAttribute("isRequired", isRequired.ToString());

            return requestedAttr;
        }
    }
}