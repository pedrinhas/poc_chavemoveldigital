<!-- SAML -->
    <add key="SAML.Version" value="2.0" />
    <add key="SAML.Destination" value="https://preprod.autenticacao.gov.pt/fa/Default.aspx" />
    <add key="SAML.Consent" value="urn:oasis:names:tc:SAML:2.0:consent:unspecified" />
    <add key="SAML.ProtocolBinding" value="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" />
    <add key="SAML.AssertionConsumerServiceURL" value="http://localhost:64181/HandleResponse" />
    <add key="SAML.ProviderName" value="UTAD" />
    <add key="SAML.Issuer" value="urn:oasis:names:tc:SAML:2.0:assertion" />
    <add key="SAML.Issuer.Value" value="http://localhost:64181" />
    <add key="SAML.Action" value="https://preprod.autenticacao.gov.pt/fa/Default.aspx" />
    <add key="SAML.NameFormat" value="urn:oasis:names:tc:SAML:2.0:attrname-format:uri" />
    <add key="SAML.FAAALevel" value="3" />
    