﻿using ChaveMovelDigital_NetFramework.Models;
using ChaveMovelDigital_NetFramework.Saml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using System.Xml;
using System.Xml.Serialization;

namespace ChaveMovelDigital_NetFramework.Controllers
{

    public class HandleResponse : IHttpHandler, IRequiresSessionState
    {
        string nic = string.Empty, nif = string.Empty, nomeCompleto = string.Empty;
        XmlReader reader;
        string relayState, requestData;


        public RequestContext RequestContext { get; set; }

        public HandleResponse() : this(null)
        {

        }

        public HandleResponse(RequestContext reqcon)
        {
            RequestContext = reqcon;
        }

        public void ProcessRequest(HttpContext context)
        {
            relayState = context.Request.Form["RelayState"];
            requestData = context.Request.Form["SAMLResponse"];

            // testar se pedido foi executado via HTTP POST:
            if (context.Request.HttpMethod != "POST")
            {
                // Pedido inválido
                // TODO: redireccionar para página de erro/login
                throw new Exception("Pedido inválido: não efectuado via HTTP POST");
            }

            if (string.IsNullOrEmpty(requestData))
            {
                throw new Exception("Recebido pedido de autenticapção inválido (SAMLResponse vazio)");
            }
            byte[] reqDataB64 = Convert.FromBase64String(requestData);
            string reqData = Encoding.UTF8.GetString(reqDataB64);
            XmlDocument xml = new XmlDocument();
            xml.PreserveWhitespace = true;

            try
            {
                xml.LoadXml(reqData);
            }
            catch (System.Xml.XmlException ex)
            {
                throw new Exception("Excepção ao carregar xml: " + ex.ToString());
            }

            reader = new XmlTextReader(new StringReader(xml.OuterXml));

            #region schema validation
            //// exemplo de validação do xml obtido:
            //string path = HttpContext.Current.Server.MapPath(".");
            //path = path.Replace('\\', '/');
            //if (!path.EndsWith("/"))
            //    path = path + "/../";
            //XmlSchemaSet schemaSet = new XmlSchemaSet();
            //schemaSet.Add("http://www.w3.org/2000/09/xmldsig#", path + "SAML/Schemas/xmldsig-core-schema.xsd");
            //schemaSet.Add("http://www.w3.org/2001/04/xmlenc#", path + "SAML/Schemas/xenc-schema.xsd");
            //schemaSet.Add("urn:oasis:names:tc:SAML:2.0:assertion", path + "SAML/Schemas/saml-schema-assertion-2.0.xsd");
            //schemaSet.Add("urn:oasis:names:tc:SAML:2.0:protocol", path + "SAML/Schemas/saml-schema-protocol-2.0.xsd");
            //schemaSet.Compile();
            //xml.Schemas = schemaSet;
            //// Sets the Xml validator event handler (if it's fired then the schema has error)
            //ValidationEventHandler validator = delegate (object obj, ValidationEventArgs args)
            //{
            //    throw new Exception("Erro na validação das schemas: " + args.Message);
            //};
            //// validates the document
            //xml.Validate(validator);
            #endregion

            #region signature validation
            string certificateB64 = xml.GetElementsByTagName("X509Certificate", "http://www.w3.org/2000/09/xmldsig#").Item(0).InnerText;
            X509Certificate2 certificate = new X509Certificate2(Convert.FromBase64String(certificateB64));

            var someBytes = certificate.Export(X509ContentType.Cert);
            using (var fs = new FileStream(@"C:\Users\Cláudio\desktop\someCert.cer", FileMode.Create))
            {
                fs.Write(someBytes, 0, someBytes.Length);
            }

            var chain = new X509Chain();
            chain.ChainPolicy.RevocationFlag = X509RevocationFlag.ExcludeRoot;
            chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
            chain.ChainPolicy.VerificationFlags = X509VerificationFlags.NoFlag;
            // sets the timeout for retrieving the certificate validation
            chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
            if (!chain.Build(certificate))
            {
                //Se falhar aqui pode ser porque o CA do certificado da resposta não está instalado. Exportar o CA e instalar na store de Trusted Root Certification Authorities *do computador*
                throw new Exception("Assinatura tem certificado inválido. O CA pode não estar instalado.");
            }
            if (!xml.PreserveWhitespace)
            {
                throw new Exception("SAMLRequest não preserva espaços em branco");
            }
            SignedXml signedXmlForValidation = new SignedXml(xml);
            XmlNodeList nodeList = xml.GetElementsByTagName("Signature", "http://www.w3.org/2000/09/xmldsig#");
            if (nodeList.Count == 0)
            {
                throw new Exception("SAMLRequest não está assinado.");
            }
            signedXmlForValidation.LoadXml((XmlElement)nodeList[0]);
            if (!signedXmlForValidation.CheckSignature())
            {
                throw new Exception("SAMLRequest tem assinatura inválida.");
            }
            #endregion

            #region 
            //// verificar schemas - NOTA: ao fazer deploy tem que ser incluida pasta SAML
            //string path = HttpContext.Current.Server.MapPath(".");
            //path = path.Replace('\\', '/');
            //if (!path.EndsWith("/"))
            //    path = path + "/";
            //XmlSchemaSet schemaSet = new XmlSchemaSet();
            //schemaSet.Add("http://www.w3.org/2000/09/xmldsig#", path + "../SAML/Schemas/xmldsig-core-schema.xsd");
            //schemaSet.Add("http://www.w3.org/2001/04/xmlenc#", path + "../SAML/Schemas/xenc-schema.xsd");
            //schemaSet.Add("urn:oasis:names:tc:SAML:2.0:assertion", path + "../SAML/Schemas/saml-schema-assertion-2.0.xsd");
            //schemaSet.Add("urn:oasis:names:tc:SAML:2.0:protocol", path + "../SAML/Schemas/saml-schema-protocol-2.0.xsd");
            //schemaSet.Compile();
            //xml.Schemas = schemaSet;
            //// Sets the Xml validator event handler (if it's fired then the schema has error)
            //ValidationEventHandler validator = delegate(object sender, ValidationEventArgs e)
            //{
            //    throw new Exception("Erro na validação das schemas: " + e.Message);
            //};
            //// validates the document
            //xml.Validate(validator);
            #endregion
            #region
            //// verificar assinatura
            //string certificateB64 = xml.GetElementsByTagName("X509Certificate", "http://www.w3.org/2000/09/xmldsig#").Item(0).InnerText;
            //X509Certificate2 certificate = new X509Certificate2(Convert.FromBase64String(certificateB64));
            //var chain = new X509Chain();
            //chain.ChainPolicy.RevocationFlag = X509RevocationFlag.ExcludeRoot;
            //chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
            //chain.ChainPolicy.VerificationFlags = X509VerificationFlags.NoFlag;
            //// sets the timeout for retrieving the certificate validation
            //chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
            //if (!chain.Build(certificate))
            //{
            //    throw new Exception("Assinatura tem certificado inválido");
            //}
            //if (!xml.PreserveWhitespace)
            //{
            //    throw new Exception("SamlResponse não preserva espaços em branco");
            //}
            //SignedXml signedXml = new SignedXml(xml);
            //XmlNodeList nodeList = xml.GetElementsByTagName("Signature", "http://www.w3.org/2000/09/xmldsig#");
            //if (nodeList.Count == 0)
            //{
            //    throw new Exception("SamlResponse não está assinado.");
            //}
            //signedXml.LoadXml((XmlElement)nodeList[0]);
            //if (!signedXml.CheckSignature())
            //{
            //    throw new Exception("SAML Response tem assinatura inválida.");
            //}
            #endregion

            // detectar tipo recebido:
            switch (xml.DocumentElement.LocalName.ToUpper())
            {
                case "RESPONSE": processResponse(xml, context); break;
                case "LOGOUTRESPONSE": processLogoutResponse(xml, context); break;
                default:
                    // tipo de resposta desconhecido ou não processável...
                    throw new Exception("Formato de mensagem desconhecido: " + xml.DocumentElement.LocalName);
            }
        }

        private void processLogoutResponse(XmlDocument xml)
        {
            // desserializar xml para LogoutRequestType
            XmlSerializer serializer = new XmlSerializer(typeof(LogoutResponseType));
            LogoutResponseType response = (LogoutResponseType)serializer.Deserialize(reader);

            // verificar validade temporal:
            int validTimeFrame = 5;
            if (Math.Abs(response.IssueInstant.Subtract(DateTime.UtcNow).TotalMinutes) > validTimeFrame)
            {
                throw new Exception("SAML Response fora do intervalo de validade - validade da resposta: " + response.IssueInstant);
            }

            // TODO: efectar restantes verificações da origem, do ID a que se refere a resposta, etc

            if ("urn:oasis:names:tc:SAML:2.0:status:Success".CompareTo(response.Status.StatusCode.Value) != 0)
            {
                // TODO: redireccionar para página de login...
                throw new Exception("Autenticação sem sucesso: " + response.Status.StatusCode.Value + " - " + response.Status.StatusMessage);
            }
        }

        private void processLogoutResponse(XmlDocument xml, HttpContext context)
        {
            processLogoutResponse(xml);

            context.Response.Redirect("~/Default.aspx");

            context.Response.End();
        }

        private bool processResponse(XmlDocument xml)
        {
            try
            {
                // desserializar xml para ResponseType
                XmlSerializer serializer = new XmlSerializer(typeof(ResponseType));
                ResponseType response = (ResponseType)serializer.Deserialize(reader);

                // verificar validade temporal:
                int validTimeFrame = 5;
                //if (Math.Abs(response.IssueInstant.Subtract(DateTime.Now).TotalMinutes) > validTimeFrame) //todo: daylight savings?
                if (Math.Abs(response.IssueInstant.Subtract(DateTime.UtcNow).TotalMinutes) > validTimeFrame)
                {
                    throw new Exception("SAML Response fora do intervalo de validade - validade da resposta: " + response.IssueInstant);
                }

                relayState = System.Text.UTF8Encoding.UTF8.GetString(System.Convert.FromBase64String(relayState));

                if (response.Status.StatusCode.Value.Equals("urn:oasis:names:tc:SAML:2.0:status:Success"))
                {
                    AssertionType assertion = new AssertionType();
                    for (int i = 0; i < response.Items.Length; i++)
                    {
                        if (response.Items[i].GetType() == typeof(AssertionType))
                        {
                            assertion = (AssertionType)response.Items[i];
                            break;
                        }
                    }

                    // validade da asserção:
                    DateTime now = DateTime.UtcNow;
                    TimeSpan tSpan = new TimeSpan(0, 0, 150); // 2,5 minutos
                    if (now < assertion.Conditions.NotBefore.Subtract(tSpan) ||
                        now >= assertion.Conditions.NotOnOrAfter.Add(tSpan))
                    {
                        // Asserção inválida 
                        // TODO: redireccionar para página de erro/login
                        throw new Exception("Asserções temporalmente inválidas.");
                    }

                    AttributeStatementType attrStatement = new AttributeStatementType();
                    for (int i = 0; i < assertion.Items.Length; i++)
                    {
                        if (assertion.Items[i].GetType() == typeof(AttributeStatementType))
                        {
                            attrStatement = (AttributeStatementType)assertion.Items[i];
                            break;
                        }
                    }

                    foreach (object obj in attrStatement.Items)
                    {
                        AttributeType attr = (AttributeType)obj;

                        if (attr.AnyAttr != null)
                        {
                            for (int i = 0; i < attr.AnyAttr.Length; i++)
                            {
                                XmlAttribute xa = attr.AnyAttr[i];
                                if (xa.LocalName.Equals("AttributeStatus") && xa.Value.Equals("Available"))
                                {
                                    if (attr.AttributeValue != null && attr.AttributeValue.Length > 0)
                                        if (((string)attr.Name).Equals("http://interop.gov.pt/MDC/Cidadao/NIC"))
                                            nic = (string)attr.AttributeValue[0];
                                        else if (((string)attr.Name).Equals("http://interop.gov.pt/MDC/Cidadao/NomeCompleto"))
                                            nomeCompleto = (string)attr.AttributeValue[0];
                                        else if (((string)attr.Name).Equals("http://interop.gov.pt/MDC/Cidadao/NIF"))
                                            nif = (string)attr.AttributeValue[0];
                                }
                            }
                        }
                    }

                    AuthHelper.NIC = nic;
                    AuthHelper.NIF = nif;
                    AuthHelper.NomeCompleto = nomeCompleto;
                    AuthHelper.Cargo = new List<AuthHelper.Cargos>() { AuthHelper.Cargos.AUTENTICADO };

                    //TODO: comparar autenticacao CC com registo de utilizadores da UTAD
                    //      selecionar cargo correto para o utilizador

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void processResponse(XmlDocument xml, HttpContext context)
        {
            var isSuccess = processResponse(xml);

            if (isSuccess)
            {
                
                context.Session.Add("alertMessage", "Autenticação efectuada com successo!");
                context.Session.Add("alertType", 1);
                context.Response.Redirect("~/Home/Index");
                context.Response.End();

            }
            else
            {
                context.Session.Add("alertMessage", "Autenticação falhou. Por favor tente novamente.");
                context.Session.Add("alertType", 3);
                context.Response.Redirect("~/Home/Index");
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}