﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChaveMovelDigital_NetFramework.Models
{
    public static class AuthHelper
    {
        #region attrs

        public static string Username
        {
            get
            {
                return HttpContext.Current.Session["username"] != null ? HttpContext.Current.Session["username"].ToString() : string.Empty;
            }
            set
            {
                HttpContext.Current.Session.Add("username", value);
            }
        }

        public static string NIF
        {
            get
            {
                return HttpContext.Current.Session["nif"] != null ? HttpContext.Current.Session["nif"].ToString() : string.Empty;
            }
            set
            {
                HttpContext.Current.Session.Add("nif", value);
            }
        }

        public static string NIC
        {
            get
            {
                return HttpContext.Current.Session["nic"] != null ? HttpContext.Current.Session["nic"].ToString() : string.Empty;
            }
            set
            {
                HttpContext.Current.Session.Add("nic", value);
            }
        }

        public static string NomeCompleto
        {
            get
            {
                return HttpContext.Current.Session["nome_completo"] != null ? HttpContext.Current.Session["nome_completo"].ToString() : string.Empty;
            }
            set
            {
                HttpContext.Current.Session.Add("nome_completo", value);
            }
        }

        public static string IUPI
        {
            get
            {
                return HttpContext.Current.Session["iupi"] != null ? HttpContext.Current.Session["iupi"].ToString() : string.Empty;
            }
            set
            {
                HttpContext.Current.Session.Add("iupi", value);
            }
        }

        public static List<Cargos> Cargo
        {
            get
            {
                return HttpContext.Current.Session["cargo"] != null ? (List<Cargos>)HttpContext.Current.Session["cargo"] : new List<Cargos>();
            }
            set
            {
                HttpContext.Current.Session.Add("cargo", value);
            }
        }

        public static string Culture
        {
            get
            {
                return System.Web.HttpContext.Current.Session["culture"] != null ? System.Web.HttpContext.Current.Session["culture"].ToString() : "pt-PT";
            }
            set
            {
                HttpContext.Current.Session.Add("culture", value);
            }
        }

        #endregion

        #region mths

        public static bool Login(string username, string password)
        {
            //TODO: proof of concept

            return !string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password);
        }

        public static void SimulateLogin()
        {
            Username = "admin";
            NIC = "12379049";
            NIF = "223311839";
            NomeCompleto = "David Gomes";
            Cargo = new List<Cargos>();// { Cargos.DPO };
        }

        public static void Logout()
        {
            //TODO: logout CC

            HttpContext.Current.Session.Clear();
        }

        public static bool IsLoggedIn
        {
            get
            {
                return Cargo != null && Cargo.Any();
            }
        }

        public static AuthenticatedModel ToAuthenticatedModel()
        {
            return new AuthenticatedModel()
            {
                NIC = NIC,
                NIF = NIF,
                Name = NomeCompleto
            };
        }

        #endregion

        #region authorize attributes

        public class LoggedInAttribute : AuthorizeAttribute
        {
            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                return IsLoggedIn;
            }

            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                filterContext.Controller.TempData["warning"] = "Acesso inválido. Deve efetuar login para aceder a esta funcionalidade.";
                filterContext.HttpContext.Session["returnURL"] = filterContext.HttpContext.Request.FilePath;

                filterContext.Result = new RedirectResult("~/Sessao/Index");
            }
        }

        public class DPOAttribute : AuthorizeAttribute
        {
            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                return Cargo.Contains(Cargos.DPO);
            }

            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                filterContext.Controller.TempData["warning"] = "Acesso inválido. Deve efetuar login para aceder a esta funcionalidade.";
                filterContext.HttpContext.Session["returnURL"] = filterContext.HttpContext.Request.FilePath;

                filterContext.Result = new RedirectResult("~/Sessao/Index");
            }
        }

        public class UserServicoAttribute : AuthorizeAttribute
        {
            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                return Cargo.Contains(Cargos.USER_SERVICO);
            }

            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                filterContext.Controller.TempData["warning"] = "Acesso inválido. Deve efetuar login para aceder a esta funcionalidade.";
                filterContext.HttpContext.Session["returnURL"] = filterContext.HttpContext.Request.FilePath;

                filterContext.Result = new RedirectResult("~/Sessao/Index");
            }
        }

        public class UTADUserAttribute : AuthorizeAttribute
        {
            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                return Cargo.Intersect(new[] { Cargos.ALUNO, Cargos.FUNCIONARIO, Cargos.DOCENTE }).Any();
            }

            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                filterContext.Controller.TempData["warning"] = "Acesso inválido. Deve efetuar login para aceder a esta funcionalidade.";
                filterContext.HttpContext.Session["returnURL"] = filterContext.HttpContext.Request.FilePath;

                filterContext.Result = new RedirectResult("~/Sessao/Index");
            }
        }

        #endregion


        public enum Cargos
        {
            DPO,
            USER_SERVICO,
            ALUNO,
            DOCENTE,
            FUNCIONARIO,
            AUTENTICADO
        }
    }
}