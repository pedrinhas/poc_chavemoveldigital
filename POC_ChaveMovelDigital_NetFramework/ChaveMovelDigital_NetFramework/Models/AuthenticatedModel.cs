﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChaveMovelDigital_NetFramework.Models
{
    public class AuthenticatedModel
    {
        public string Name { get; set; }
        public string NIC { get; set; }
        public string NIF { get; set; }
    }
}