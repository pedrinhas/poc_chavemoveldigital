﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChaveMovelDigital_NetFramework.Models
{
    public class AuthSessionModel
    {

    }

    public class AuthRequestModel
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class AuthResponseModel
    {
        public string username { get; set; }
        public string nome { get; set; }
        public string iupi { get; set; }
        public ResponseStatusModel ResponseInfo { get; set; }
    }

    public class ResponseStatusModel
    {
        public string ErrorMessage { get; set; }
        public bool Success { get; set; }
    }

    public class RoleResponseModel
    {
        public string CodigoOrgao { get; set; }
        public string CodigoTipoCargo { get; set; }
        public string Iupi { get; set; }
        public string Orgao { get; set; }
        public string Pessoa { get; set; }
        public string TipoCargo { get; set; }
    }

    public class ProfileResponseModel
    {
        public int? Id { get; set; }
        public int? Ano { get; set; }
        public string Categoria { get; set; }
        public int? CodigoCategoria { get; set; }
        public int? CodigoUnidadeOrganica { get; set; }
        public string Tipo { get; set; }
        public string UnidadeOrganica { get; set; }
    }
}