﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace ChaveMovelDigital_NetFramework.Models
{
    public class AppSettings
    {
        public string assertionConsumerServiceUrl = "http://localhost:8910/CCManagement/Consume";
        public string issuer = "test-app";
    }
}