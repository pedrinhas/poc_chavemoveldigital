﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChaveMovelDigital_NetFramework.Models
{
    public class AuthSAMLRequestModel
    {
        public string Action { get; set; }
        public string RelayState { get; set; }
        public string SAMLRequest { get; set; }
    }
}