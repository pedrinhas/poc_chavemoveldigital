﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace ChaveMovelDigital_NetFramework.Helpers
{
    public static class ConfigurationHelper
    {
        public static class AutenticacaoGovPt
        {
            public static bool IsProd => _getValue<bool>("AutenticacaoGovPt.IsProd");

            public static class SAML
            {
                public static string Version => _getValue("AutenticacaoGovPt.SAML.Version");
                public static string Destination => _getValue($"AutenticacaoGovPt.SAML.Destination{(IsProd ? "" : "PreProd")}");
                public static string Consent => _getValue("AutenticacaoGovPt.SAML.Consent");
                public static string ProtocolBinding => _getValue("AutenticacaoGovPt.SAML.ProtocolBinding");
                public static string AssertionConsumerServiceURL => _getValue("AutenticacaoGovPt.SAML.AssertionConsumerServiceURL");
                public static string ProviderName => _getValue("AutenticacaoGovPt.SAML.ProviderName");
                public static string Issuer => _getValue("AutenticacaoGovPt.SAML.Issuer");
                public static string IssuerValue => _getValue("AutenticacaoGovPt.SAML.IssuerValue");
                public static string Action => _getValue($"AutenticacaoGovPt.SAML.Action{(IsProd ? "" : "PreProd")}");
                public static string NameFormat => _getValue("AutenticacaoGovPt.SAML.NameFormat");
                public static string FAAALevel => _getValue("AutenticacaoGovPt.SAML.FAAALevel");
            }

            public static class REST
            {
                public static string PedidoAutorizacao => _getValue($"AutenticacaoGovPt.REST.PedidoAutorizacao{(IsProd ? "" : "PreProd")}");
                public static string PedidoDados => _getValue($"AutenticacaoGovPt.REST.PedidoDados{(IsProd ? "" : "PreProd")}");
            }
        }

        public static class Certificates
        {
            public static string RequestCertificatePath = _getPath("Certificates.RequestCertificatePath");
            public static string RequestCertificatePassword = _getValue("Certificates.RequestCertificatePassword");
        }

        private static T _getValue<T>(string configKey)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(configKey))
                {
                    throw new ArgumentNullException(nameof(configKey));
                }

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(configKey))
                {
                    throw new KeyNotFoundException();
                }

                var valueAsString = ConfigurationManager.AppSettings[configKey];
                var converted = Convert.ChangeType(valueAsString, typeof(T));

                return (T)converted;
            }
            catch (Exception ex)
            {
                //TODO: log this
                return default;
            }
        }

        private static string _getValue(string configKey) => _getValue<string>(configKey);

        private static string _getPath(string configKey)
        {
            try
            {
                var val = _getValue(configKey);

                if (string.IsNullOrWhiteSpace(val))
                {
                    throw new InvalidOperationException($"Value for config key {configKey} is null or empty");
                }

                var path = System.Web.Hosting.HostingEnvironment.MapPath(val);

                return path;
            }
            catch (Exception ex)
            {
                //TODO: log this
                return null;
            }
        }
    }
}