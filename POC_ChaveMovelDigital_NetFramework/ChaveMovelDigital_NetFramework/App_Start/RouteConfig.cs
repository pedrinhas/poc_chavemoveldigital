﻿using ChaveMovelDigital_NetFramework.App_Start;
using ChaveMovelDigital_NetFramework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ChaveMovelDigital_NetFramework
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.Add("HandleResponse", new Route("HandleResponse", new RouteValueDictionary() { { "controller", "HandleResponse" } }, new RouteHandler()));

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = nameof(HomeController.Send), id = UrlParameter.Optional }
            );
        }
    }
}
