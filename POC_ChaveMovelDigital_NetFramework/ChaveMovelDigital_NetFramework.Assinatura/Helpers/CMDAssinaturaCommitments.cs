﻿namespace ChaveMovelDigital_NetFramework.Assinatura.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    /// Defines the CMDAssinaturaCommitments.
    /// </summary>
    public enum CMDAssinaturaCommitments
    {
        /// <summary>
        /// Indica que o assinante reconhece ter criado, aprovado e enviado os dados assinados
        /// OID 1.2.840.113549.1.9.16.6.1
        /// </summary>
        ProofOfOrigin,
        /// <summary>
        /// Indica que o assinante aprovou o conteúdo dos dados assinados.
        /// OID 1.2.840.113549.1.9.16.6.5
        /// </summary>
        ProofOfApproval,
        /// <summary>
        /// Indica que o assinante criou os dados assinados (não significa necessariamente que os aprovou ou enviou).
        /// OID 1.2.840.113549.1.9.16.6.6
        /// </summary>
        ProofOfCreation,
        /// <summary>
        /// Indica que a assinatura foi criada com a intenção de autenticar os dados assinados.
        /// OID 2.16.620.2.1.3.1
        /// </summary>
        DataAuthentication,
        /// <summary>
        /// Indica que a assinatura foi criada com a intenção de autenticar a entidade que assina.
        /// OID 2.16.620.2.1.3.2
        /// </summary>
        EntityAuthentication,
        /// <summary>
        /// Indica que a assinatura foi criada com a intenção de indicar autoria dos dados assinados.
        /// OID 2.16.620.2.1.3.3
        /// </summary>
        Authorship,
        /// <summary>
        /// Indica que a assinatura foi criada com a intenção de indicar a revisão dos dados assinados.
        /// OID 2.16.620.2.1.3.4
        /// </summary>
        Review,
        /// <summary>
        /// Indica que a assinatura foi criada com a intenção de indicar que o documento é uma cópia do original (em papel ou eletrónico).
        /// OID 2.16.620.2.1.3.5
        /// </summary>
        Copy,
        /// <summary>
        /// Indica que a assinatura foi criada com a intenção de indicar que o assinante é testemunha que a(s) pessoa(s) que assinaram os mesmos dados com o compromisso <see cref="CMDAssinaturaCommitments.BoundToDataSigned"/> (OID 2.16.620.2.1.3.7), leram, aprovaram e estão vinculados ao conteúdo dos dados assinados.
        /// OID 2.16.620.2.1.3.6
        /// </summary>
        SignatureWitness,
        /// <summary>
        /// Indica que a assinatura foi criada com a intenção de indicar que o assinante leu, aprovou e está vinculado ao conteúdo dos dados assinados.
        /// OID 2.16.620.2.1.3.7
        /// </summary>
        BoundToDataSigned,
        /// <summary>
        /// Indica que a assinatura foi criada com a intenção de indicar uma aprovação intermédia, como parte de um processo de decisão.
        /// OID 2.16.620.2.1.3.8
        /// </summary>
        IntermediateApproval,
        /// <summary>
        /// Compromisso por defeito quando não se enquadra nas outras. Não oficial.
        /// </summary>
        None
    }

    /// <summary>
    /// Defines the <see cref="CMDAssinaturaCommitmentsExtensions" />.
    /// </summary>
    public static class CMDAssinaturaCommitmentsExtensions
    {
        /// <summary>
        /// Defines the __DEFAULT_OID.
        /// </summary>
        private const string __DEFAULT_OID = null;

        /// <summary>
        /// Defines the __DEFAULT_REASON.
        /// </summary>
        private const CMDAssinaturaCommitments __DEFAULT_REASON = CMDAssinaturaCommitments.None;

        /// <summary>
        /// Defines the commitmentToOid.
        /// </summary>
        private static IReadOnlyDictionary<CMDAssinaturaCommitments, string> commitmentToOid => new ReadOnlyDictionary<CMDAssinaturaCommitments, string>(oidToCommitment.ToDictionary(k => k.Value, v => v.Key));

        /// <summary>
        /// Defines the oidToCommitment.
        /// </summary>
        private static readonly IReadOnlyDictionary<string, CMDAssinaturaCommitments> oidToCommitment = new ReadOnlyDictionary<string, CMDAssinaturaCommitments>(new Dictionary<string, CMDAssinaturaCommitments>()
        {
            { "1.2.840.113549.1.9.16.6.1", CMDAssinaturaCommitments.ProofOfOrigin },
            { "1.2.840.113549.1.9.16.6.5", CMDAssinaturaCommitments.ProofOfApproval },
            { "1.2.840.113549.1.9.16.6.6", CMDAssinaturaCommitments.ProofOfCreation },
            { "2.16.620.2.1.3.1", CMDAssinaturaCommitments.DataAuthentication },
            { "2.16.620.2.1.3.2", CMDAssinaturaCommitments.EntityAuthentication },
            { "2.16.620.2.1.3.3", CMDAssinaturaCommitments.Authorship },
            { "2.16.620.2.1.3.4", CMDAssinaturaCommitments.Review },
            { "2.16.620.2.1.3.5", CMDAssinaturaCommitments.Copy },
            { "2.16.620.2.1.3.6", CMDAssinaturaCommitments.SignatureWitness },
            { "2.16.620.2.1.3.7", CMDAssinaturaCommitments.BoundToDataSigned },
            { "2.16.620.2.1.3.8", CMDAssinaturaCommitments.IntermediateApproval }
        });
        /// <summary>
        /// The ParseOid.
        /// </summary>
        /// <param name="oid">The oid<see cref="string"/>.</param>
        /// <returns>The <see cref="CMDAssinaturaCommitments"/>.</returns>
        public static CMDAssinaturaCommitments ParseOid(string oid)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(oid))
                {
                    throw new ArgumentNullException(nameof(oid));
                }

                if (oidToCommitment.ContainsKey(oid))
                {
                    return oidToCommitment[oid];
                }

                return __DEFAULT_REASON;
            }
            catch (Exception ex)
            {
                return __DEFAULT_REASON;
            }
        }

        /// <summary>
        /// The ToFriendlyName.
        /// </summary>
        /// <param name="commitment">The commitment<see cref="CMDAssinaturaCommitments"/>.</param>
        /// <returns>The <see cref="string"/>.</returns>
        public static string ToFriendlyName(this CMDAssinaturaCommitments commitment)
        {
            switch (commitment)
            {
                case CMDAssinaturaCommitments.ProofOfOrigin: return "Prova de origem";
                case CMDAssinaturaCommitments.ProofOfApproval: return "Prova de aprovação";
                case CMDAssinaturaCommitments.ProofOfCreation: return "Prova de criação";
                case CMDAssinaturaCommitments.DataAuthentication: return "Autenticação de dados";
                case CMDAssinaturaCommitments.EntityAuthentication: return "Autenticação de entidade";
                case CMDAssinaturaCommitments.Authorship: return "Autoria";
                case CMDAssinaturaCommitments.Review: return "Revisão";
                case CMDAssinaturaCommitments.Copy: return "Cópia";
                case CMDAssinaturaCommitments.SignatureWitness: return "Testemunha de assinatura";
                case CMDAssinaturaCommitments.BoundToDataSigned: return "Vinculação ao conteúdo assinado";
                case CMDAssinaturaCommitments.IntermediateApproval: return "Aprovação intermédia";
                case CMDAssinaturaCommitments.None:
                default:
                    return null;
            }
        }

        /// <summary>
        /// The ToFullName.
        /// </summary>
        /// <param name="commitment">The commitment<see cref="CMDAssinaturaCommitments"/>.</param>
        /// <returns>The <see cref="string"/>.</returns>
        public static string ToFullName(this CMDAssinaturaCommitments commitment) => $"{ToFriendlyName(commitment)} ({ToOid(commitment)})";

        /// <summary>
        /// The ToOid.
        /// </summary>
        /// <param name="commitment">The commitment<see cref="CMDAssinaturaCommitments"/>.</param>
        /// <returns>The <see cref="string"/>.</returns>
        public static string ToOid(this CMDAssinaturaCommitments commitment)
        {
            try
            {
                if (commitmentToOid.ContainsKey(commitment))
                {
                    return commitmentToOid[commitment];
                }

                return __DEFAULT_OID;
            }
            catch (Exception ex)
            {
                return __DEFAULT_OID;
            }
        }
    }
}
