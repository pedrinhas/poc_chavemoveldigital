﻿namespace ChaveMovelDigital_NetFramework.Assinatura.Helpers
{
    using ChaveMovelDigital_NetFramework.Assinatura.iTextExtensions;
    using iText.Forms;
    using iText.Forms.Fields;
    using iText.IO.Font;
    using iText.IO.Source;
    using iText.Kernel.Font;
    using iText.Kernel.Geom;
    using iText.Kernel.Pdf;
    using iText.Kernel.Pdf.Annot;
    using iText.Kernel.Pdf.Extgstate;
    using iText.Kernel.Pdf.Xobject;
    using iText.Layout;
    using iText.Layout.Element;
    using iText.Layout.Layout;
    using iText.Layout.Renderer;
    using iText.Signatures;
    using Org.BouncyCastle.Asn1.X509;
    using Org.BouncyCastle.Crypto;
    using Org.BouncyCastle.OpenSsl;
    using Org.BouncyCastle.Security;
    using Org.BouncyCastle.X509;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="CMDAssinaturaHelper" />.
    /// </summary>
    public static class CMDAssinaturaHelper
    {
        /// <summary>
        /// The ThrowIfParameterIsNull.
        /// </summary>
        /// <param name="value">The value<see cref="string"/>.</param>
        /// <param name="paramName">The paramName<see cref="string"/>.</param>
        private static void ThrowIfParameterIsNull(string value, string paramName)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(paramName);
            }
        }

        /// <summary>
        /// The ThrowIfParameterIsNull.
        /// </summary>
        /// <param name="value">The value<see cref="object"/>.</param>
        /// <param name="paramName">The paramName<see cref="string"/>.</param>
        private static void ThrowIfParameterIsNull(object value, string paramName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(paramName);
            }
        }

        /// <summary>
        /// Defines the <see cref="Conversions" />.
        /// </summary>
        public static class Conversions
        {
            /// <summary>
            /// Defines the __default_encoding.
            /// </summary>
            private static readonly Encoding __default_encoding = new UTF8Encoding();

            /// <summary>
            /// The ByteArrayToBase64.
            /// </summary>
            /// <param name="value">The value<see cref="byte[]"/>.</param>
            /// <returns>The <see cref="string"/>.</returns>
            public static string ByteArrayToBase64(byte[] value)
            {
                try
                {
                    ThrowIfParameterIsNull(value, nameof(value));

                    return InternalByteArrayToBase64(value);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The StringToByteArray.
            /// </summary>
            /// <param name="value">The value<see cref="string"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] StringToByteArray(string value)
            {
                try
                {
                    ThrowIfParameterIsNull(value, nameof(value));

                    return InternalStringToByteArray(value);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The StringToByteArray.
            /// </summary>
            /// <param name="value">The value<see cref="string"/>.</param>
            /// <param name="encoding">The encoding<see cref="Encoding"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] StringToByteArray(string value, Encoding encoding)
            {
                try
                {
                    ThrowIfParameterIsNull(value, nameof(value));
                    ThrowIfParameterIsNull(encoding, nameof(encoding));

                    return InternalStringToByteArray(value, encoding);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            
            /// <summary>
            /// The InternalByteArrayToBase64.
            /// </summary>
            /// <param name="value">The value<see cref="byte[]"/>.</param>
            /// <returns>The <see cref="string"/>.</returns>
            private static string InternalByteArrayToBase64(byte[] value)
            {
                return Convert.ToBase64String(value);
            }

            /// <summary>
            /// The InternalStringToByteArray.
            /// </summary>
            /// <param name="value">The value<see cref="string"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            private static byte[] InternalStringToByteArray(string value)
            {
                return InternalStringToByteArray(value, __default_encoding);
            }

            /// <summary>
            /// The InternalStringToByteArray.
            /// </summary>
            /// <param name="value">The value<see cref="string"/>.</param>
            /// <param name="encoding">The encoding<see cref="Encoding"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            private static byte[] InternalStringToByteArray(string value, Encoding encoding)
            {
                return encoding.GetBytes(value);
            }
        }

        /// <summary>
        /// Defines the <see cref="Crypto" />.
        /// </summary>
        public static class Crypto
        {
            /// <summary>
            /// The EncryptWithCertToBase64.
            /// </summary>
            /// <param name="certPath">The certPath<see cref="string"/>.</param>
            /// <param name="certThumbprint">The certThumbprint<see cref="string"/>.</param>
            /// <param name="data">The data<see cref="byte[]"/>.</param>
            /// <returns>The <see cref="string"/>.</returns>
            public static string EncryptWithCertToBase64(string certPath, string certThumbprint, byte[] data)
            {
                try
                {
                    ThrowIfParameterIsNull(certPath, nameof(certPath));
                    ThrowIfParameterIsNull(certThumbprint, nameof(certThumbprint));
                    ThrowIfParameterIsNull(data, nameof(data));

                    var encrypted = InternalEncryptWithCert(certPath, certThumbprint, data);

                    return Conversions.ByteArrayToBase64(encrypted);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The EncryptWithCertToBase64.
            /// </summary>
            /// <param name="certPath">The certPath<see cref="string"/>.</param>
            /// <param name="certThumbprint">The certThumbprint<see cref="string"/>.</param>
            /// <param name="data">The data<see cref="string"/>.</param>
            /// <returns>The <see cref="string"/>.</returns>
            public static string EncryptWithCertToBase64(string certPath, string certThumbprint, string data)
            {
                try
                {
                    ThrowIfParameterIsNull(certPath, nameof(certPath));
                    ThrowIfParameterIsNull(certThumbprint, nameof(certThumbprint));
                    ThrowIfParameterIsNull(data, nameof(data));

                    var asBytes = Conversions.StringToByteArray(data);

                    var encrypted = InternalEncryptWithCert(certPath, certThumbprint, asBytes);

                    return Conversions.ByteArrayToBase64(encrypted);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The ReadChainFromPemBouncyCastle.
            /// </summary>
            /// <param name="pem">The pem<see cref="string"/>.</param>
            /// <returns>The <see cref="IEnumerable{Org.BouncyCastle.X509.X509Certificate}"/>.</returns>
            public static IEnumerable<Org.BouncyCastle.X509.X509Certificate> ReadChainFromPemBouncyCastle(string pem)
            {
                try
                {
                    ThrowIfParameterIsNull(pem, nameof(pem));

                    return InternalReadChainFromPemBouncyCastle(pem);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The InternalEncryptWithCert.
            /// </summary>
            /// <param name="certPath">The certPath<see cref="string"/>.</param>
            /// <param name="certThumbprint">The certThumbprint<see cref="string"/>.</param>
            /// <param name="data">The data<see cref="byte[]"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            private static byte[] InternalEncryptWithCert(string certPath, string certThumbprint, byte[] data)
            {
                try
                {
                    if (!File.Exists(certPath))
                    {
                        throw new ArgumentException("Certificate file not found", nameof(certPath));
                    }

                    var certs = new X509Certificate2Collection();
                    certs.Import(certPath);

                    foreach (var cert in certs)
                    {
                        if (cert.Thumbprint.ToLowerInvariant() == certThumbprint.ToLowerInvariant())
                        {
                            using (var rsa = cert.GetRSAPublicKey())
                            {
                                return rsa.Encrypt(data, RSAEncryptionPadding.Pkcs1);
                            }
                        }
                    }

                    throw new KeyNotFoundException($"The certificate with thumbprint \"{certThumbprint}\" was not found");
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The InternalReadChainFromPemBouncyCastle.
            /// </summary>
            /// <param name="pem">The pem<see cref="string"/>.</param>
            /// <returns>The <see cref="IEnumerable{Org.BouncyCastle.X509.X509Certificate}"/>.</returns>
            private static IEnumerable<Org.BouncyCastle.X509.X509Certificate> InternalReadChainFromPemBouncyCastle(string pem)
            {
                string certBegin = "-----BEGIN CERTIFICATE-----";

                var pemCerts = pem.Split(new string[] { certBegin }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var pemCert in pemCerts)
                {
                    using (var sr = new StringReader((certBegin + pemCert).Trim()))
                    {
                        var pemReader = new PemReader(sr);
                        var pemObject = pemReader.ReadPemObject();

                        var reader = new X509CertificateParser();

                        var certs = reader.ReadCertificates(pemObject.Content);

                        foreach (var c in certs)
                        {
                            yield return (Org.BouncyCastle.X509.X509Certificate)c;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Defines the <see cref="PDF" />.
        /// </summary>
        public static class PDF
        {
            /// <summary>
            /// Defines the __commitments_label
            /// </summary>
            private const string __commitments_label = "Compromissos";

            /// <summary>
            /// Defines the __date_time_format.
            /// </summary>
            private const string __date_time_format = "yyyy-MM-dd HH:mm:ss zzz";

            /// <summary>
            /// Defines the __default_date_caption.
            /// </summary>
            private const string __default_date_caption = "Data";

            /// <summary>
            /// Defines the __default_font.
            /// </summary>
            private const string __default_font = @"c:\Windows\Fonts\arial.ttf";

            /// <summary>
            /// Defines the __default_location_caption.
            /// </summary>
            private const string __default_location_caption = "Local";

            /// <summary>
            /// Defines the __default_reason_caption.
            /// </summary>
            private const string __default_reason_caption = "Motivo";

            /// <summary>
            /// Defines the __default_signature_watermark_transparency.
            /// </summary>
            private const float __default_signature_watermark_transparency = 0.3f;

            /// <summary>
            /// Defines the __default_signed_by_caption.
            /// </summary>
            private const string __default_signed_by_caption = "Assinado por";

            /// <summary>
            /// The starting font size. This will increase or diminish depending to fill the rectangle.....
            /// </summary>
            private const int __starting_font_size = 1;

            /// <summary>
            /// Defines the __default_tsa_servers.
            /// </summary>
            private static readonly SortedList<int, TSAMultiClient.TSAMultiClientServerInfo> __default_tsa_servers = new SortedList<int, TSAMultiClient.TSAMultiClientServerInfo>();
            
            /// <summary>
            /// Gets the DefaultHashAlgorithm.
            /// </summary>
            public static string DefaultHashAlgorithm => DigestAlgorithms.SHA256;

            /// <summary>
            /// Gets the DefaultTSAServers
            /// The default TSA servers to be used when no TSA client is provided.
            /// </summary>
            public static IReadOnlyDictionary<int, TSAMultiClient.TSAMultiClientServerInfo> DefaultTSAServers
            {
                get
                {
                    return new ReadOnlyDictionary<int, TSAMultiClient.TSAMultiClientServerInfo>(__default_tsa_servers.OrderBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value));
                }
            }
            /// <summary>
            /// Adds a default TSA server info to the end of the list (lowest priority).
            /// </summary>
            /// <param name="tsaServerInfo">.</param>
            /// <returns>.</returns>
            public static IReadOnlyDictionary<int, TSAMultiClient.TSAMultiClientServerInfo> AddDefaultTSAServer(TSAMultiClient.TSAMultiClientServerInfo tsaServerInfo)
            {
                return AddDefaultTSAServer(__default_tsa_servers.Count, tsaServerInfo);
            }

            /// <summary>
            /// Adds a default TSA server info to the list at the desired <paramref name="index"/>.
            /// </summary>
            /// <param name="index">.</param>
            /// <param name="tsaServerInfo">.</param>
            /// <returns>.</returns>
            public static IReadOnlyDictionary<int, TSAMultiClient.TSAMultiClientServerInfo> AddDefaultTSAServer(int index, TSAMultiClient.TSAMultiClientServerInfo tsaServerInfo)
            {
                try
                {
                    if (index < -1)
                    {
                        throw new ArgumentException("Index cannot be less than -1", nameof(index));
                    }

                    ThrowIfParameterIsNull(tsaServerInfo, nameof(tsaServerInfo));

                    if (string.IsNullOrWhiteSpace(tsaServerInfo.URL))
                    {
                        throw new ArgumentException($"Member {nameof(TSAMultiClient.TSAMultiClientServerInfo.URL)} of parameter {nameof(tsaServerInfo)} cannot be null, empty or whitespace", nameof(TSAMultiClient.TSAMultiClientServerInfo.URL));
                    }

                    if (!__default_tsa_servers.ContainsKey(index) && !__default_tsa_servers.ContainsValue(tsaServerInfo))
                    {
                        __default_tsa_servers.Add(index, tsaServerInfo);
                    }

                    return DefaultTSAServers;
                }
                catch (Exception ex)
                {
                    //TODO: log this
                    throw;
                }
            }

            /// <summary>
            /// The AddSignatureField.
            /// </summary>
            /// <param name="pdf">The pdf<see cref="byte[]"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AddSignatureField(byte[] pdf, CMDAssinaturaOptions options)
            {
                try
                {
                    ThrowIfParameterIsNull(pdf, nameof(pdf));
                    ThrowIfParameterIsNull(options, nameof(options));

                    if (options.Appearance == null || !options.Appearance.Visible)
                    {
                        return pdf;
                    }

                    return InternalAddSignatureField(pdf, options.SignatureFieldName, options.Appearance.PageNumber, options.Appearance.SignatureFieldRectangle);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The AddSignatureField.
            /// </summary>
            /// <param name="pdf">.</param>
            /// <param name="options">.</param>
            /// <returns>.</returns>
            public static byte[] AddSignatureField(Stream pdf, CMDAssinaturaOptions options)
            {
                try
                {
                    ThrowIfParameterIsNull(pdf, nameof(pdf));
                    ThrowIfParameterIsNull(options, nameof(options));

                    using (var ms = new MemoryStream())
                    {
                        if (pdf.CanSeek)
                        {
                            pdf.Position = 0;
                        }

                        pdf.CopyTo(ms);

                        if (options.Appearance == null || !options.Appearance.Visible)
                        {
                            return ms.ToArray();
                        }

                        return InternalAddSignatureField(ms.ToArray(), options.SignatureFieldName, options.Appearance.PageNumber, options.Appearance.SignatureFieldRectangle);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The AddSignatureField.
            /// </summary>
            /// <param name="pdf">The pdf<see cref="byte[]"/>.</param>
            /// <param name="signatureFieldName">The signatureFieldName<see cref="string"/>.</param>
            /// <param name="pageNumber">The pageNumber<see cref="int"/>.</param>
            /// <param name="rectangle">The rectangle<see cref="Rectangle"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AddSignatureField(byte[] pdf, string signatureFieldName, int pageNumber, Rectangle rectangle)
            {
                try
                {
                    ThrowIfParameterIsNull(pdf, nameof(pdf));
                    ThrowIfParameterIsNull(signatureFieldName, nameof(signatureFieldName));
                    ThrowIfParameterIsNull(rectangle, nameof(rectangle));

                    return InternalAddSignatureField(pdf, signatureFieldName, pageNumber, rectangle);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The AddSignatureField.
            /// </summary>
            /// <param name="pdf">The pdf<see cref="Stream"/>.</param>
            /// <param name="signatureFieldName">The signatureFieldName<see cref="string"/>.</param>
            /// <param name="pageNumber">The pageNumber<see cref="int"/>.</param>
            /// <param name="rectangle">The rectangle<see cref="Rectangle"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AddSignatureField(Stream pdf, string signatureFieldName, int pageNumber, Rectangle rectangle)
            {
                try
                {
                    ThrowIfParameterIsNull(pdf, nameof(pdf));
                    ThrowIfParameterIsNull(signatureFieldName, nameof(signatureFieldName));
                    ThrowIfParameterIsNull(rectangle, nameof(rectangle));

                    using (var ms = new MemoryStream())
                    {
                        if (pdf.CanSeek)
                        {
                            pdf.Position = 0;
                        }

                        pdf.CopyTo(ms);

                        return InternalAddSignatureField(ms.ToArray(), signatureFieldName, pageNumber, rectangle);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The AppendSignature.
            /// </summary>
            /// <param name="preSignResult">The preSignResult<see cref="PreSignResult"/>.</param>
            /// <param name="signedHash">The signedHash<see cref="byte[]"/>.</param>
            /// <param name="pemCertificate">The pemCertificate<see cref="string"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendSignature(PreSignResult preSignResult, byte[] signedHash, string pemCertificate)
            {
                return AppendSignature(preSignResult, signedHash, pemCertificate, DefaultTSAServers.Values);
            }

            /// <summary>
            /// The AppendSignature.
            /// </summary>
            /// <param name="preSignResult">The preSignResult<see cref="PreSignResult"/>.</param>
            /// <param name="signedHash">The signedHash<see cref="byte[]"/>.</param>
            /// <param name="pemCertificate">The pemCertificate<see cref="string"/>.</param>
            /// <param name="tsaServers">The tsaServers<see cref="IEnumerable{TSAMultiClient.TSAMultiClientServerInfo}"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendSignature(PreSignResult preSignResult, byte[] signedHash, string pemCertificate, IEnumerable<TSAMultiClient.TSAMultiClientServerInfo> tsaServers)
            {
                return AppendSignature(preSignResult, signedHash, pemCertificate, null, null, tsaServers);
            }

            /// <summary>
            /// The AppendSignature.
            /// </summary>
            /// <param name="preSignResult">The preSignResult<see cref="PreSignResult"/>.</param>
            /// <param name="signedHash">The signedHash<see cref="byte[]"/>.</param>
            /// <param name="pemCertificate">The pemCertificate<see cref="string"/>.</param>
            /// <param name="tsaClient">The tsaClient<see cref="ITSAClient"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendSignature(PreSignResult preSignResult, byte[] signedHash, string pemCertificate, ITSAClient tsaClient)
            {
                return AppendSignature(preSignResult, signedHash, pemCertificate, null, null, tsaClient);
            }

            /// <summary>
            /// The AppendSignature.
            /// </summary>
            /// <param name="preSignResult">The preSignResult<see cref="PreSignResult"/>.</param>
            /// <param name="signedHash">The signedHash<see cref="byte[]"/>.</param>
            /// <param name="pemCertificate">The pemCertificate<see cref="string"/>.</param>
            /// <param name="location">The location<see cref="string"/>.</param>
            /// <param name="reason">The reason<see cref="string"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendSignature(PreSignResult preSignResult, byte[] signedHash, string pemCertificate, string location, string reason)
            {
                return AppendSignature(preSignResult, signedHash, pemCertificate, location, reason, DefaultTSAServers.Values);
            }

            /// <summary>
            /// The AppendSignature.
            /// </summary>
            /// <param name="preSignResult">The preSignResult<see cref="PreSignResult"/>.</param>
            /// <param name="signedHash">The signedHash<see cref="byte[]"/>.</param>
            /// <param name="pemCertificate">The pemCertificate<see cref="string"/>.</param>
            /// <param name="location">The location<see cref="string"/>.</param>
            /// <param name="reason">The reason<see cref="string"/>.</param>
            /// <param name="tsaServers">The tsaServers<see cref="IEnumerable{TSAMultiClient.TSAMultiClientServerInfo}"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendSignature(PreSignResult preSignResult, byte[] signedHash, string pemCertificate, string location, string reason, IEnumerable<TSAMultiClient.TSAMultiClientServerInfo> tsaServers)
            {
                try
                {
                    ThrowIfParameterIsNull(pemCertificate, nameof(pemCertificate));

                    return AppendSignature(preSignResult, signedHash, new CMDAssinaturaOptions()
                    {
                        Reason = reason,
                        Location = location
                    }.ReadChainFromString(pemCertificate), tsaServers);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The AppendSignature.
            /// </summary>
            /// <param name="preSignResult">The preSignResult<see cref="PreSignResult"/>.</param>
            /// <param name="signedHash">The signedHash<see cref="byte[]"/>.</param>
            /// <param name="pemCertificate">The pemCertificate<see cref="string"/>.</param>
            /// <param name="location">The location<see cref="string"/>.</param>
            /// <param name="reason">The reason<see cref="string"/>.</param>
            /// <param name="tsaClient">The tsaClient<see cref="ITSAClient"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendSignature(PreSignResult preSignResult, byte[] signedHash, string pemCertificate, string location, string reason, ITSAClient tsaClient)
            {
                try
                {
                    ThrowIfParameterIsNull(pemCertificate, nameof(pemCertificate));

                    return AppendSignature(preSignResult, signedHash, new CMDAssinaturaOptions()
                    {
                        Reason = reason,
                        Location = location
                    }.ReadChainFromString(pemCertificate), tsaClient);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The AppendSignature.
            /// </summary>
            /// <param name="preSignResult">The preSignResult<see cref="PreSignResult"/>.</param>
            /// <param name="signedHash">The signedHash<see cref="byte[]"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendSignature(PreSignResult preSignResult, byte[] signedHash, CMDAssinaturaOptions options)
            {
                return AppendSignature(preSignResult, signedHash, options, DefaultTSAServers.Values);
            }

            /// <summary>
            /// The AppendSignature.
            /// </summary>
            /// <param name="preSignResult">The preSignResult<see cref="PreSignResult"/>.</param>
            /// <param name="signedHash">The signedHash<see cref="byte[]"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <param name="tsaServers">The tsaServers<see cref="IEnumerable{TSAMultiClient.TSAMultiClientServerInfo}"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendSignature(PreSignResult preSignResult, byte[] signedHash, CMDAssinaturaOptions options, IEnumerable<TSAMultiClient.TSAMultiClientServerInfo> tsaServers)
            {
                try
                {
                    ITSAClient tsa = null;

                    try
                    {
                        tsa = new TSAMultiClient(tsaServers);
                    }
                    catch (Exception ex)
                    {
                    }

                    return AppendSignature(preSignResult, signedHash, options, tsa);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The AppendSignature.
            /// </summary>
            /// <param name="preSignResult">The preSignResult<see cref="PreSignResult"/>.</param>
            /// <param name="signedHash">The signedHash<see cref="byte[]"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <param name="tsaClient">The tsaClient<see cref="ITSAClient"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendSignature(PreSignResult preSignResult, byte[] signedHash, CMDAssinaturaOptions options, ITSAClient tsaClient)
            {
                try
                {
                    ThrowIfParameterIsNull(preSignResult, nameof(preSignResult));
                    ThrowIfParameterIsNull(signedHash, nameof(signedHash));
                    ThrowIfParameterIsNull(options, nameof(options));

                    return InternalAppendSignature(preSignResult, signedHash, options, tsaClient);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The AppendTimestamp.
            /// </summary>
            /// <param name="document">The document<see cref="byte[]"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendTimestamp(byte[] document)
            {
                return AppendTimestamp(document, DefaultTSAServers.Values);
            }

            /// <summary>
            /// The AppendTimestamp.
            /// </summary>
            /// <param name="document">The document<see cref="byte[]"/>.</param>
            /// <param name="tsaServers">The tsaServers<see cref="IEnumerable{TSAMultiClient.TSAMultiClientServerInfo}"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendTimestamp(byte[] document, IEnumerable<TSAMultiClient.TSAMultiClientServerInfo> tsaServers)
            {
                try
                {
                    ITSAClient tsa = null;

                    try
                    {
                        tsa = new TSAMultiClient(tsaServers);
                    }
                    catch (Exception ex)
                    {
                    }

                    return AppendTimestamp(document, tsa);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The AppendTimestamp.
            /// </summary>
            /// <param name="document">The document<see cref="byte[]"/>.</param>
            /// <param name="tsaClient">The tsaClient<see cref="ITSAClient"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            public static byte[] AppendTimestamp(byte[] document, ITSAClient tsaClient)
            {
                try
                {
                    ThrowIfParameterIsNull(document, nameof(document));
                    ThrowIfParameterIsNull(tsaClient, nameof(tsaClient));

                    return InternalAppendTimestamp(document, tsaClient);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The PreSignDocument.
            /// </summary>
            /// <param name="pdf">The pdf<see cref="byte[]"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <returns>The <see cref="PreSignResult"/>.</returns>
            public static PreSignResult PreSignDocument(byte[] pdf, CMDAssinaturaOptions options)
            {
                return PreSignDocument(pdf, DefaultHashAlgorithm, options);
            }

            /// <summary>
            /// The PreSignDocument.
            /// </summary>
            /// <param name="pdf">The pdf<see cref="byte[]"/>.</param>
            /// <param name="alg">The alg<see cref="string"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <returns>The <see cref="PreSignResult"/>.</returns>
            public static PreSignResult PreSignDocument(byte[] pdf, string alg, CMDAssinaturaOptions options)
            {
                try
                {
                    ThrowIfParameterIsNull(pdf, nameof(pdf));
                    ThrowIfParameterIsNull(alg, nameof(alg));
                    ThrowIfParameterIsNull(options, nameof(options));

                    return InternalPreSignDocument(pdf, alg, options);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The PreSignDocument.
            /// </summary>
            /// <param name="pdf">The pdf<see cref="Stream"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <returns>The <see cref="PreSignResult"/>.</returns>
            public static PreSignResult PreSignDocument(Stream pdf, CMDAssinaturaOptions options)
            {
                return PreSignDocument(pdf, DefaultHashAlgorithm, options);
            }

            /// <summary>
            /// The PreSignDocument.
            /// </summary>
            /// <param name="pdf">The pdf<see cref="Stream"/>.</param>
            /// <param name="alg">The alg<see cref="string"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <returns>The <see cref="PreSignResult"/>.</returns>
            public static PreSignResult PreSignDocument(Stream pdf, string alg, CMDAssinaturaOptions options)
            {
                try
                {
                    ThrowIfParameterIsNull(pdf, nameof(pdf));
                    ThrowIfParameterIsNull(alg, nameof(alg));
                    ThrowIfParameterIsNull(options, nameof(options));

                    using (var ms = new MemoryStream())
                    {
                        if (pdf.CanSeek)
                        {
                            pdf.Position = 0;
                        }

                        pdf.CopyTo(ms);

                        return InternalPreSignDocument(ms.ToArray(), alg, options);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// Removes a default TSA server info.
            /// </summary>
            /// <param name="index">.</param>
            /// <returns>.</returns>
            public static IReadOnlyDictionary<int, TSAMultiClient.TSAMultiClientServerInfo> RemoveDefaultTSAServer(int index)
            {
                try
                {
                    if (index < -1)
                    {
                        throw new ArgumentException("Index cannot be less than -1", nameof(index));
                    }

                    if (__default_tsa_servers.ContainsKey(index))
                    {
                        __default_tsa_servers.Remove(index);
                    }

                    return DefaultTSAServers;
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
            /// <summary>
            /// The AddLTV.
            /// </summary>
            /// <param name="docBytes">The docBytes<see cref="byte[]"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <param name="tsa">The tsa<see cref="ITSAClient"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            private static byte[] AddLTV(byte[] docBytes, CMDAssinaturaOptions options, ITSAClient tsa)
            {
                try
                {
                    var bytes = (byte[])null;

                    using (var reader = new PdfReader(new MemoryStream(docBytes)))
                    using (var output = new MemoryStream())
                    using (var writer = new PdfWriter(output))
                    using (var doc = new PdfDocument(reader, writer, DefaultStampingProperties()))
                    {
                        var ltv = new LtvVerification(doc);
                        var signUtil = new SignatureUtil(doc);

                        var lastSignName = signUtil.GetSignatureNames().Last();
                        var lastSign = signUtil.ReadSignatureData(lastSignName);

                        var ocsp = GetOCSPClient();

                        if (lastSign.IsTsp())
                        {
                            var tsaChain = lastSign.GetSignCertificateChain();
                            var tsaCrl = GetCRLClient(tsaChain);

                            ltv.AddVerification(lastSignName, ocsp, tsaCrl, LtvVerification.CertificateOption.WHOLE_CHAIN, LtvVerification.Level.OCSP_OPTIONAL_CRL, LtvVerification.CertificateInclusion.YES);
                        }

                        var chain = options.Chain;
                        var crl = GetCRLClient(options.Chain.ToArray());

                        ltv.AddVerification(options.SignatureFieldName, ocsp, crl, LtvVerification.CertificateOption.WHOLE_CHAIN, LtvVerification.Level.OCSP_OPTIONAL_CRL, LtvVerification.CertificateInclusion.YES);

                        ltv.Merge();

                        bytes = output.ToArray();
                    }

                    if (tsa != null && options.AppendTimestampSignature)
                    {
                        //if a tsa is provided we add a timestamp signature

                        bytes = InternalAppendTimestamp(bytes, tsa);
                    }

                    return bytes;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The CreateFont.
            /// </summary>
            /// <param name="fontName">The fontName<see cref="string"/>.</param>
            /// <returns>The <see cref="PdfFont"/>.</returns>
            private static PdfFont CreateFont(string fontName)
            {
                var fontProgram = FontProgramFactory.CreateFont(fontName);

                return PdfFontFactory.CreateFont(fontProgram, "Windows-1252", true);
            }

            /// <summary>
            /// The CreateNewLineText.
            /// </summary>
            /// <param name="fontName">The fontName<see cref="string"/>.</param>
            /// <param name="fontSize">The fontSize<see cref="int"/>.</param>
            /// <returns>The <see cref="Text"/>.</returns>
            private static Text CreateNewLineText(string fontName, int fontSize)
            {
                return new Text(Environment.NewLine)
                    .SetFont(CreateFont(fontName))
                    .SetFontSize(fontSize);
            }

            /// <summary>
            /// The CreateSignatureParagraph.
            /// </summary>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <param name="signatureDate">The signatureDate<see cref="DateTime"/>.</param>
            /// <param name="fontSize">The fontSize<see cref="int"/>.</param>
            /// <returns>The <see cref="Paragraph"/>.</returns>
            private static Paragraph CreateSignatureParagraph(CMDAssinaturaOptions options, DateTime signatureDate, int fontSize)
            {
                var reasonCaption = options.ReasonCaption?.Trim(':', ' ');
                var locationCaption = options.LocationCaption?.Trim(':', ' ');
                var font = options.Appearance?.Font;

                if (string.IsNullOrWhiteSpace(reasonCaption))
                {
                    reasonCaption = __default_reason_caption;
                }
                if (string.IsNullOrWhiteSpace(locationCaption))
                {
                    locationCaption = __default_location_caption;
                }
                if (string.IsNullOrWhiteSpace(font))
                {
                    font = __default_font;
                }

                var text = new Paragraph();

                var newLine = CreateNewLineText(font, fontSize);

                if (!string.IsNullOrWhiteSpace(options.Name))
                {
                    var nameLabel = CreateSignatureTextLabel(__default_signed_by_caption, font, fontSize);
                    var nameText = CreateSignatureText(options.Name, font, fontSize);

                    text.Add(nameLabel).Add(nameText).Add(newLine);
                }

                var dateLabel = CreateSignatureTextLabel(__default_date_caption, font, fontSize);
                var dateText = CreateSignatureText(signatureDate.ToString(__date_time_format), font, fontSize);

                text.Add(dateLabel).Add(dateText).Add(newLine);

                if (!string.IsNullOrWhiteSpace(options.Location))
                {
                    var locationLabel = CreateSignatureTextLabel(locationCaption, font, fontSize);
                    var location = CreateSignatureText(options.Location, font, fontSize);

                    text.Add(locationLabel).Add(location);
                }

                if (!string.IsNullOrWhiteSpace(options.Reason))
                {
                    var reasonLabel = CreateSignatureTextLabel(reasonCaption, font, fontSize);
                    var reason = CreateSignatureText(options.Reason, font, fontSize);

                    text.Add(reasonLabel).Add(reason).Add(newLine);
                }

                if (options.Appearance.SignatureFieldTextMargins != null)
                {
                    text.SetMarginTop(options.Appearance.SignatureFieldTextMargins.Top);
                    text.SetMarginBottom(options.Appearance.SignatureFieldTextMargins.Bottom);
                    text.SetMarginLeft(options.Appearance.SignatureFieldTextMargins.Left);
                    text.SetMarginRight(options.Appearance.SignatureFieldTextMargins.Right);
                }

                return text;
            }

            /// <summary>
            /// The CreateSignatureText.
            /// </summary>
            /// <param name="text">The text<see cref="string"/>.</param>
            /// <param name="fontName">The fontName<see cref="string"/>.</param>
            /// <param name="fontSize">The fontSize<see cref="int"/>.</param>
            /// <returns>The <see cref="Text"/>.</returns>
            private static Text CreateSignatureText(string text, string fontName, int fontSize)
            {
                return new Text(text)
                    .SetFont(CreateFont(fontName))
                    .SetFontSize(fontSize);
            }

            /// <summary>
            /// The CreateSignatureTextLabel.
            /// </summary>
            /// <param name="label">The label<see cref="string"/>.</param>
            /// <param name="fontName">The fontName<see cref="string"/>.</param>
            /// <param name="fontSize">The fontSize<see cref="int"/>.</param>
            /// <returns>The <see cref="Text"/>.</returns>
            private static Text CreateSignatureTextLabel(string label, string fontName, int fontSize)
            {
                return new Text($"{label.Trim(':', ' ')}: ")
                    .SetFont(CreateFont(fontName))
                    .SetFontSize(fontSize)
                    .SetBold();
            }

            /// <summary>
            /// The DefaultStampingProperties.
            /// </summary>
            /// <returns>The <see cref="StampingProperties"/>.</returns>
            private static StampingProperties DefaultStampingProperties()
            {
                var props = new StampingProperties();
                props.UseAppendMode();

                return props;
            }
            /// <summary>
            /// The GetCRLClient.
            /// </summary>
            /// <param name="chain">The chain<see cref="IEnumerable{Org.BouncyCastle.X509.X509Certificate}"/>.</param>
            /// <returns>The <see cref="ICrlClient"/>.</returns>
            private static ICrlClient GetCRLClient(IEnumerable<Org.BouncyCastle.X509.X509Certificate> chain) => new CrlClientOnline(chain?.ToArray());

            /// <summary>
            /// The GetCRLResponse.
            /// </summary>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <returns>The <see cref="ICollection{byte[]}"/>.</returns>
            private static ICollection<byte[]> GetCRLResponse(CMDAssinaturaOptions options)
            {
                try
                {
                    var crlList = new List<byte[]>();

                    if (options?.Chain != null && options.Chain.Any())
                    {
                        var crlClient = GetCRLClient(options.Chain);

                        var obtainedCrl = crlClient.GetEncoded(options.SigningCertificate, null);

                        crlList.AddRange(obtainedCrl);
                    }

                    return crlList.Any() ? crlList : null;
                }
                catch (Exception)
                {
                    //TODO: log this
                    return null;
                }
            }

            /// <summary>
            /// The GetOCSPClient.
            /// </summary>
            /// <returns>The <see cref="IOcspClient"/>.</returns>
            private static IOcspClient GetOCSPClient() => new OcspClientBouncyCastle(null);

            /// <summary>
            /// The GetOCSPResponse.
            /// </summary>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <returns>The <see cref="ICollection{byte[]}"/>.</returns>
            private static ICollection<byte[]> GetOCSPResponse(CMDAssinaturaOptions options)
            {
                try
                {
                    var ocspList = new List<byte[]>();

                    if (options?.Chain != null && options.Chain.Count() > 1)
                    {
                        var chain = options.Chain.ToList();
                        var ocspClient = GetOCSPClient();

                        //can only test certs with parent
                        for (int j = 0; j < chain.Count() - 1; ++j)
                        {
                            byte[] ocsp = ocspClient.GetEncoded(chain[j], chain[j + 1], null);
                            if (ocsp != null)
                            {
                                ocspList.Add(ocsp);
                            }
                        }
                    }

                    return ocspList.Any() ? ocspList : null;
                }
                catch (Exception ex)
                {
                    //TODO: log this
                    return null;
                }
            }

            /// <summary>
            /// The InternalAddBlankSignature.
            /// </summary>
            /// <param name="pdf">The pdf<see cref="byte[]"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            private static byte[] InternalAddBlankSignature(byte[] pdf, CMDAssinaturaOptions options)
            {
                try
                {
                    using (var inputStream = new MemoryStream(pdf))
                    using (var reader = new PdfReader(inputStream))
                    using (var os = new MemoryStream())
                    {
                        var stampingProperties = DefaultStampingProperties();

                        var signer = new PdfSigner(reader, os, stampingProperties);

                        var doc = signer.GetDocument();
                        var util = new SignatureUtil(doc);
                        var fieldAlreadySigned = false;
                        var fieldExists = util.DoesSignatureFieldExist(options.SignatureFieldName);

                        if (fieldExists)
                        {
                            var sig = util.GetSignature(options.SignatureFieldName);

                            fieldAlreadySigned = sig != null;

                            if (fieldAlreadySigned)
                            {
                                throw new ArgumentException($"The signature field \"{options.SignatureFieldName}\" is already signed. Please provide an empty signature field name or a new name");
                            }
                        }

                        var appearance = signer.GetSignatureAppearance();

                        InternalApplyOptionsToAppearance(appearance, doc, options, signer.GetSignDate());

                        signer.SetFieldName(options.SignatureFieldName);

                        var external = new ExternalBlankSignatureContainer(PdfName.Adobe_PPKLite, PdfName.Adbe_pkcs7_detached);

                        var ocspResponse = GetOCSPResponse(options);
                        var crlResponse = GetCRLResponse(options);

                        var estimatedSize = 8192; //estimated size
                        estimatedSize += ocspResponse == null ? 0 : ocspResponse.Sum(x => x.Length); //ocsp part
                        estimatedSize += crlResponse == null ? 0 : crlResponse.Sum(x => x.Length); //crl part
                        estimatedSize += 4096; //estimated tsa part

                        signer.SignExternalContainer(external, estimatedSize);

                        return os.ToArray();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The InternalAddSignatureField.
            /// </summary>
            /// <param name="pdf">The pdf<see cref="byte[]"/>.</param>
            /// <param name="signatureFieldName">The signatureFieldName<see cref="string"/>.</param>
            /// <param name="pageNumber">The pageNumber<see cref="int"/>.</param>
            /// <param name="rectangle">The rectangle<see cref="Rectangle"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            private static byte[] InternalAddSignatureField(byte[] pdf, string signatureFieldName, int pageNumber, Rectangle rectangle)
            {
                try
                {
                    var os = new MemoryStream();

                    ////PDFA
                    //using (var inputStream = new MemoryStream(pdf))
                    //using (var reader = new PdfReader(inputStream))
                    //using (var writer = new PdfWriter(os))
                    //using (var doc = new PdfADocument(reader, writer))
                    using (var inputStream = new MemoryStream(pdf))
                    using (var reader = new PdfReader(inputStream))
                    using (var writer = new PdfWriter(os))
                    using (var doc = new PdfDocument(reader, writer))
                    {
                        var form = PdfAcroForm.GetAcroForm(doc, true);

                        ////PDFA
                        //var signField = PdfFormField.CreateSignature(doc, options.Appearance.SignatureFieldRectangle, PdfAConformanceLevel.PDF_A_3A);
                        var signField = PdfFormField.CreateSignature(doc, rectangle);

                        signField.SetFieldName(signatureFieldName);

                        PdfPage signPage;

                        try
                        {
                            signPage = doc.GetPage(pageNumber);
                        }
                        catch (Exception ex)
                        {
                            signPage = doc.GetLastPage();
                        }

                        form.AddField(signField, signPage);

                        PdfAnnotation annotation = doc.GetLastPage().GetAnnotations().First();
                        PdfFormXObject appearObj = new PdfFormXObject(rectangle);
                        PdfAnnotationAppearance appearance = new PdfAnnotationAppearance(appearObj.GetPdfObject());
                        annotation.SetNormalAppearance(appearance);
                    }

                    return os.ToArray();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            
            /// <summary>
            /// The InternalAppendSignature.
            /// </summary>
            /// <param name="preSignResult">The preSignResult<see cref="PreSignResult"/>.</param>
            /// <param name="signedHash">The signedHash<see cref="byte[]"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <param name="tsa">The tsa<see cref="ITSAClient"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            private static byte[] InternalAppendSignature(PreSignResult preSignResult, byte[] signedHash, CMDAssinaturaOptions options, ITSAClient tsa)
            {
                try
                {
                    var bytes = (byte[])null;

                    using (var inputStream = new MemoryStream(preSignResult.PreSignedDocument))
                    using (var reader = new PdfReader(inputStream))
                    using (var doc = new PdfDocument(reader))
                    using (var os = new MemoryStream())
                    {
                        var ocsp = GetOCSPResponse(options);
                        var crl = GetCRLResponse(options);

                        var externalSignature = new CMDExternalSignatureContainer(signedHash, preSignResult.DocumentHash, options.Chain, tsa, ocsp, null);

                        //var stampingProperties = DefaultStampingProperties();

                        //var signer = new PdfSigner(reader, os, stampingProperties);

                        //InternalApplyOptionsToAppearance(signer.GetSignatureAppearance(), options);

                        PdfSigner.SignDeferred(doc, options.SignatureFieldName, os, externalSignature);

                        bytes = os.ToArray();
                    }

                    var ltvBytes = AddLTV(bytes, options, tsa);

                    return ltvBytes;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// The InternalAppendTimestamp.
            /// </summary>
            /// <param name="document">The document<see cref="byte[]"/>.</param>
            /// <param name="tsaClient">The tsaClient<see cref="ITSAClient"/>.</param>
            /// <returns>The <see cref="byte[]"/>.</returns>
            private static byte[] InternalAppendTimestamp(byte[] document, ITSAClient tsaClient)
            {
                try
                {
                    var bytes = (byte[])null;

                    using (var reader = new PdfReader(new MemoryStream(document)))
                    using (var output = new MemoryStream())
                    {
                        var signer = new PdfSigner(reader, output, DefaultStampingProperties());
                        signer.Timestamp(tsaClient, null);

                        bytes = output.ToArray();
                    }

                    return bytes;
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            /// <summary>
            /// The InternalApplyOptionsToAppearance.
            /// </summary>
            /// <param name="appearance">The appearance<see cref="PdfSignatureAppearance"/>.</param>
            /// <param name="doc">The doc<see cref="PdfDocument"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <param name="signatureDate">The signatureDate<see cref="DateTime"/>.</param>
            /// <returns>The <see cref="PdfSignatureAppearance"/>.</returns>
            private static PdfSignatureAppearance InternalApplyOptionsToAppearance(PdfSignatureAppearance appearance, PdfDocument doc, CMDAssinaturaOptions options, DateTime signatureDate)
            {
                var reasonCaption = options.ReasonCaption?.Trim(':', ' ');
                var locationCaption = options.LocationCaption?.Trim(':', ' ');

                if (string.IsNullOrWhiteSpace(reasonCaption))
                {
                    reasonCaption = __default_reason_caption;
                }
                if (string.IsNullOrWhiteSpace(locationCaption))
                {
                    locationCaption = __default_location_caption;
                }

                appearance.SetReasonCaption(reasonCaption);
                appearance.SetLocationCaption(locationCaption);

                if (options.Appearance != null)
                {
                    if (options.Appearance.Visible)
                    {
                        if (options.Appearance.SignatureFieldRectangle != null)
                        {
                            appearance.SetPageRect(options.Appearance.SignatureFieldRectangle);
                        }

                        if (options.Appearance.WatermarkImage != null)
                        {
                            var image = new Image(options.Appearance.WatermarkImage);
                            image.SetAutoScale(true);
                            image.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);
                            if (options.Appearance.WatermarkMargins != null)
                            {
                                image.SetMarginTop(options.Appearance.WatermarkMargins.Top);
                                image.SetMarginBottom(options.Appearance.WatermarkMargins.Bottom);
                                image.SetMarginLeft(options.Appearance.WatermarkMargins.Left);
                                image.SetMarginRight(options.Appearance.WatermarkMargins.Right);
                            }

                            var imageDiv = new Div();

                            imageDiv.SetHeight(options.Appearance.SignatureFieldRectangle.GetHeight());
                            imageDiv.SetWidth(options.Appearance.SignatureFieldRectangle.GetWidth());
                            imageDiv.SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.MIDDLE);
                            imageDiv.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);
                            imageDiv.Add(image);

                            var watermarkContainer = new Canvas(appearance.GetLayer2(), doc);
                            var watermarkCanvas = watermarkContainer.GetPdfCanvas();
                            watermarkCanvas.SaveState();

                            var gs1 = new PdfExtGState();
                            gs1.SetFillOpacity(__default_signature_watermark_transparency);
                            watermarkCanvas.SetExtGState(gs1);

                            watermarkContainer.Add(imageDiv);

                            watermarkCanvas.RestoreState();
                        }

                        var maxFontSizeFound = false;
                        var maxFontSize = __starting_font_size;

                        do
                        {
                            var fontSize = maxFontSize + 1;

                            var testParagraph = CreateSignatureParagraph(options, signatureDate, fontSize);

                            var renderer = (ParagraphRenderer)testParagraph.CreateRendererSubTree();
                            renderer.SetParent(new DocumentRenderer(new Document(doc)));

                            var layoutResult = renderer.Layout(new iText.Layout.Layout.LayoutContext(new iText.Layout.Layout.LayoutArea(options.Appearance.PageNumber, options.Appearance.SignatureFieldRectangle)));

                            switch (layoutResult.GetStatus())
                            {
                                case LayoutResult.FULL:
                                    maxFontSize = fontSize;

                                    maxFontSizeFound = false;
                                    break;

                                case LayoutResult.NOTHING:
                                case LayoutResult.PARTIAL:
                                default:
                                    maxFontSizeFound = true;
                                    break;
                            }
                        } while (!maxFontSizeFound);

                        var text = CreateSignatureParagraph(options, signatureDate, maxFontSize);

                        var textDiv = new Div();
                        textDiv.SetHeight(options.Appearance.SignatureFieldRectangle.GetHeight());
                        textDiv.SetWidth(options.Appearance.SignatureFieldRectangle.GetWidth());
                        textDiv.SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.MIDDLE);
                        textDiv.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);
                        textDiv.Add(text);

                        var textContainer = new Canvas(appearance.GetLayer2(), doc);
                        textContainer.Add(textDiv);
                    }
                }

                var reason = options.Reason ?? "";

                if (!string.IsNullOrWhiteSpace(options.Reason) && options.Commitments.Any())
                {
                    reason += Environment.NewLine;
                }
                
                if (options.Commitments.Any())
                {
                    reason += $"{__commitments_label}: {string.Join(", ", options.Commitments.Select(x => x.ToFullName()))}";
                }

                appearance.SetReason(reason);

                appearance.SetLocation(options.Location);

                appearance.SetCertificate(options.SigningCertificate);

                return appearance;
            }
            
            /// <summary>
            /// The InternalPreSignDocument.
            /// </summary>
            /// <param name="pdf">The pdf<see cref="byte[]"/>.</param>
            /// <param name="alg">The alg<see cref="string"/>.</param>
            /// <param name="options">The options<see cref="CMDAssinaturaOptions"/>.</param>
            /// <returns>The <see cref="PreSignResult"/>.</returns>
            private static PreSignResult InternalPreSignDocument(byte[] pdf, string alg, CMDAssinaturaOptions options)
            {
                try
                {
                    var withBlank = InternalAddBlankSignature(pdf, options);

                    File.WriteAllBytes(@"d:\withBlank.pdf", withBlank);

                    using (var inputStream = new MemoryStream(withBlank))
                    using (var reader = new PdfReader(inputStream))
                    using (var doc = new PdfDocument(reader))
                    {
                        var signatureUtil = new SignatureUtil(doc);

                        var dic = signatureUtil.GetSignatureDictionary(options.SignatureFieldName);

                        var b = dic.GetAsArray(PdfName.ByteRange);
                        var gaps = b.ToLongArray();

                        var ras = reader.GetSafeFile().CreateSourceView();

                        using (var ranged = new RASInputStream(new RandomAccessSourceFactory().CreateRanged(ras, gaps)))
                        {
                            IDigest messageDigest = DigestUtilities.GetDigest(alg);
                            PdfPKCS7 sign = new PdfPKCS7(null, options.Chain.ToArray(), alg, false);

                            var hash = DigestAlgorithms.Digest(ranged, messageDigest);
                            var authenticatedBytes = sign.GetAuthenticatedAttributeBytes(hash, PdfSigner.CryptoStandard.CMS, null, null);

                            var signableHash = DigestAlgorithms.Digest(new MemoryStream(authenticatedBytes), messageDigest);

                            var oid = CryptoConfig.MapNameToOID(HashAlgorithmName.SHA256.Name);

                            return new PreSignResult(signableHash, hash, oid, withBlank);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            /// <summary>
            /// Defines the <see cref="PreSignResult" />.
            /// </summary>
            public class PreSignResult
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="PreSignResult"/> class.
                /// </summary>
                /// <param name="signableHash">The signableHash<see cref="byte[]"/>.</param>
                /// <param name="documentHash">The documentHash<see cref="byte[]"/>.</param>
                /// <param name="oid">The oid<see cref="string"/>.</param>
                /// <param name="preSignedDoc">The preSignedDoc<see cref="byte[]"/>.</param>
                public PreSignResult(byte[] signableHash, byte[] documentHash, string oid, byte[] preSignedDoc)
                {
                    this.SignableHash = signableHash;
                    this.DocumentHash = documentHash;
                    this.Oid = oid;
                    this.PreSignedDocument = preSignedDoc;
                }

                /// <summary>
                /// Gets the DocumentHash.
                /// </summary>
                public byte[] DocumentHash { get; }

                /// <summary>
                /// Gets the Oid.
                /// </summary>
                public string Oid { get; }

                /// <summary>
                /// Gets the PreSignedDocument.
                /// </summary>
                public byte[] PreSignedDocument { get; }

                /// <summary>
                /// Gets the SignableHash.
                /// </summary>
                public byte[] SignableHash { get; }
                /// <summary>
                /// Gets the SignableHashDigestInfo.
                /// </summary>
                public byte[] SignableHashDigestInfo
                {
                    get
                    {
                        if (SignableHash == null)
                        {
                            return null;
                        }

                        var di = new DigestInfo(new AlgorithmIdentifier(new Org.BouncyCastle.Asn1.DerObjectIdentifier(Oid), null), SignableHash);

                        var derEncoded = di.GetDerEncoded();
                        return derEncoded;
                    }
                }
            }
        }

        /// <summary>
        /// Defines the <see cref="StatusCodes" />.
        /// </summary>
        public static class StatusCodes
        {
            /// <summary>
            /// Defines the StatusCode.
            /// </summary>
            public enum StatusCode
            {
                /// <summary>
                /// OK.
                /// </summary>
                Ok,
                /// <summary>
                /// Erros de sistema.
                /// </summary>
                SystemError,
                /// <summary>
                /// Parâmetros inválidos.
                /// </summary>
                InvalidParameters,
                /// <summary>
                /// Pins não correspondem.
                /// </summary>
                BadPin,
                /// <summary>
                /// OTP inválido.
                /// </summary>
                BadOTP,
                /// <summary>
                /// Ocorre quando o serviço de assinatura do SCMD está inativo. O cidadão não recebe o OTP.
                /// </summary>
                SCMDServiceInactive,
                /// <summary>
                /// Erro genérico.
                /// </summary>
                GenericError,
                /// <summary>
                /// Sei lá, shit happens.
                /// </summary>
                Unknown
            }

            /// <summary>
            /// The ParseStatusCode.
            /// </summary>
            /// <param name="statusCode">The statusCode<see cref="string"/>.</param>
            /// <returns>The <see cref="StatusCode"/>.</returns>
            public static StatusCode ParseStatusCode(string statusCode)
            {
                try
                {
                    ThrowIfParameterIsNull(statusCode, nameof(statusCode));

                    var asInt = 0;

                    if (int.TryParse(statusCode, out asInt))
                    {
                        if (asInt == 200)
                        {
                            return StatusCode.Ok;
                        }
                        else if (asInt % 100 == 5) //500's
                        {
                            return StatusCode.SystemError;
                        }
                        else if (asInt == 801)
                        {
                            return StatusCode.BadPin;
                        }
                        else if (asInt == 802)
                        {
                            return StatusCode.BadOTP;
                        }
                        else if (asInt == 817)
                        {
                            return StatusCode.SCMDServiceInactive;
                        }
                        else if (asInt % 100 == 8)
                        {
                            return StatusCode.InvalidParameters;
                        }
                        else if (asInt == 900)
                        {
                            return StatusCode.GenericError;
                        }

                        return StatusCode.Unknown;
                    }
                    else
                    {
                        throw new ArgumentException($"Could not parse the status code \"{statusCode}\"", nameof(statusCode));
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
