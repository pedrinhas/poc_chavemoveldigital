﻿using System;
using System.IO;
using Common.Logging;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Ocsp;
using Org.BouncyCastle.X509;
using iText.IO.Util;
using iText.Signatures;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using iText.Kernel;
using System.Net;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Security;
using iText.Kernel.Pdf;
using System.Collections;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Asn1.Ocsp;
using Org.BouncyCastle.Tsp;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1.Esf;

namespace ChaveMovelDigital_NetFramework.Assinatura.Helpers
{
    public class OcspClientBouncyCastleSource : IOcspClient
    {
        /// <summary>The Logger instance.</summary>
        private static readonly ILog LOGGER = LogManager.GetLogger(typeof(OcspClientBouncyCastleSource)
            );

        private readonly OCSPVerifier verifier;

        /// <summary>
        /// Create
        /// <c>OcspClient</c>
        /// </summary>
        /// <param name="verifier">will be used for response verification.</param>
        /// <seealso cref="OCSPVerifier"/>
        public OcspClientBouncyCastleSource(OCSPVerifier verifier)
        {
            this.verifier = verifier;
        }

        /// <summary>Gets OCSP response.</summary>
        /// <remarks>
        /// Gets OCSP response. If
        /// <see cref="OCSPVerifier"/>
        /// was set, the response will be checked.
        /// </remarks>
        /// <param name="checkCert">to certificate to check</param>
        /// <param name="rootCert">the parent certificate</param>
        /// <param name="url">to get the verification</param>
        public virtual BasicOcspResp GetBasicOCSPResp(X509Certificate checkCert, X509Certificate rootCert, String
            url)
        {
            try
            {
                OcspResp ocspResponse = GetOcspResponse(checkCert, rootCert, url);
                if (ocspResponse == null)
                {
                    return null;
                }
                if (ocspResponse.Status != Org.BouncyCastle.Asn1.Ocsp.OcspResponseStatus.Successful)
                {
                    return null;
                }
                BasicOcspResp basicResponse = (BasicOcspResp)ocspResponse.GetResponseObject();
                if (verifier != null)
                {
                    verifier.IsValidResponse(basicResponse, rootCert);
                }
                return basicResponse;
            }
            catch (Exception ex)
            {
                LOGGER.Error(ex.Message);
            }
            return null;
        }

        /// <summary>Gets an encoded byte array with OCSP validation.</summary>
        /// <remarks>Gets an encoded byte array with OCSP validation. The method should not throw an exception.</remarks>
        /// <param name="checkCert">to certificate to check</param>
        /// <param name="rootCert">the parent certificate</param>
        /// <param name="url">
        /// to get the verification. It it's null it will be taken
        /// from the check cert or from other implementation specific source
        /// </param>
        /// <returns>a byte array with the validation or null if the validation could not be obtained</returns>
        public virtual byte[] GetEncoded(X509Certificate checkCert, X509Certificate rootCert, String url)
        {
            try
            {
                BasicOcspResp basicResponse = GetBasicOCSPResp(checkCert, rootCert, url);
                if (basicResponse != null)
                {
                    SingleResp[] responses = basicResponse.Responses;
                    if (responses.Length == 1)
                    {
                        SingleResp resp = responses[0];
                        Object status = resp.GetCertStatus();
                        if (status == CertificateStatus.Good)
                        {
                            return basicResponse.GetEncoded();
                        }
                        else
                        {
                            if (status is RevokedStatus)
                            {
                                throw new System.IO.IOException(iText.IO.LogMessageConstant.OCSP_STATUS_IS_REVOKED);
                            }
                            else
                            {
                                throw new System.IO.IOException(iText.IO.LogMessageConstant.OCSP_STATUS_IS_UNKNOWN);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOGGER.Error(ex.Message);
            }
            return null;
        }

        /// <summary>Generates an OCSP request using BouncyCastle.</summary>
        /// <param name="issuerCert">certificate of the issues</param>
        /// <param name="serialNumber">serial number</param>
        /// <returns>an OCSP request</returns>
        private static OcspReq GenerateOCSPRequest(X509Certificate issuerCert, BigInteger serialNumber)
        {
            //Add provider BC
            // Generate the id for the certificate we are looking for
            CertificateID id = SignUtils.GenerateCertificateId(issuerCert, serialNumber, Org.BouncyCastle.Ocsp.CertificateID.HashSha1
                );
            // basic request generation with nonce
            return SignUtils.GenerateOcspRequestWithNonce(id);
        }

        private OcspResp GetOcspResponse(X509Certificate checkCert, X509Certificate rootCert, String url)
        {
            if (checkCert == null || rootCert == null)
            {
                return null;
            }
            if (url == null)
            {
                url = GetOCSPURL(checkCert);
            }
            if (url == null)
            {
                return null;
            }
            LOGGER.Info("Getting OCSP from " + url);
            OcspReq request = GenerateOCSPRequest(rootCert, checkCert.SerialNumber);
            byte[] array = request.GetEncoded();
            Uri urlt = new Uri(url);
            Stream @in = SignUtils.GetHttpResponseForOcspRequest(array, urlt);
            return new OcspResp(StreamUtil.InputStreamToArray(@in));
        }

        String GetOCSPURL(X509Certificate certificate)
        {
            Asn1Object obj;
            try
            {
                obj = GetExtensionValue(certificate, X509Extensions.AuthorityInfoAccess.Id);
                if (obj == null)
                {
                    return null;
                }
                Asn1Sequence AccessDescriptions = (Asn1Sequence)obj;
                for (int i = 0; i < AccessDescriptions.Count; i++)
                {
                    Asn1Sequence AccessDescription = (Asn1Sequence)AccessDescriptions[i];
                    if (AccessDescription.Count != 2)
                    {
                        continue;
                    }
                    else
                    {
                        if (AccessDescription[0] is DerObjectIdentifier)
                        {
                            DerObjectIdentifier id = (DerObjectIdentifier)AccessDescription[0];
                            if (SecurityIDs.ID_OCSP.Equals(id.Id))
                            {
                                Asn1Object description = (Asn1Object)AccessDescription[1];
                                String AccessLocation = GetStringFromGeneralName(description);
                                if (AccessLocation == null)
                                {
                                    return "";
                                }
                                else
                                {
                                    return AccessLocation;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.IO.IOException)
            {
                return null;
            }
            return null;
        }
        private static Asn1Object GetExtensionValue(X509Certificate certificate, String oid)
        {
            byte[] bytes = SignUtils.GetExtensionValueByOid(certificate, oid);
            if (bytes == null)
            {
                return null;
            }
            Asn1InputStream aIn = new Asn1InputStream(new MemoryStream(bytes));
            Asn1OctetString octs = (Asn1OctetString)aIn.ReadObject();
            aIn = new Asn1InputStream(new MemoryStream(octs.GetOctets()));
            return aIn.ReadObject();
        }

        private static String GetStringFromGeneralName(Asn1Object names)
        {
            Asn1TaggedObject taggedObject = (Asn1TaggedObject)names;
            return iText.IO.Util.JavaUtil.GetStringForBytes(Asn1OctetString.GetInstance(taggedObject, false).GetOctets
                (), "ISO-8859-1");
        }
        class SignUtils
        {
            internal static String GetPrivateKeyAlgorithm(ICipherParameters cp)
            {
                String algorithm;
                if (cp is RsaKeyParameters)
                {
                    algorithm = "RSA";
                }
                else if (cp is DsaKeyParameters)
                {
                    algorithm = "DSA";
                }
                else if (cp is ECKeyParameters)
                {
                    algorithm = ((ECKeyParameters)cp).AlgorithmName;
                    if (algorithm == "EC")
                    {
                        algorithm = "ECDSA";
                    }
                }
                else
                {
                    throw new PdfException(PdfException.UnknownKeyAlgorithm1).SetMessageParams(cp.ToString());
                }

                return algorithm;
            }

            /// <summary>
            /// Parses a CRL from an input Stream.
            /// </summary>
            /// <param name="input">The input Stream holding the unparsed CRL.</param>
            /// <returns>The parsed CRL object.</returns>
            internal static X509Crl ParseCrlFromStream(Stream input)
            {
                return new X509CrlParser().ReadCrl(input);
            }

            internal static X509Crl ParseCrlFromUrl(String crlurl)
            {
                X509CrlParser crlParser = new X509CrlParser();
                // Creates the CRL
                Stream url = WebRequest.Create(crlurl).GetResponse().GetResponseStream();
                return crlParser.ReadCrl(url);
            }

            internal static byte[] GetExtensionValueByOid(X509Certificate certificate, String oid)
            {
                Asn1OctetString extensionValue = certificate.GetExtensionValue(oid);
                return extensionValue != null ? extensionValue.GetDerEncoded() : null;


            }

            internal static IDigest GetMessageDigest(String hashAlgorithm)
            {
                return DigestUtilities.GetDigest(hashAlgorithm);
            }

            internal static Stream GetHttpResponse(Uri urlt)
            {
                HttpWebRequest con = (HttpWebRequest)WebRequest.Create(urlt);
                HttpWebResponse response = (HttpWebResponse)con.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new PdfException(PdfException.InvalidHttpResponse1).SetMessageParams(response.StatusCode);
                return response.GetResponseStream();
            }

            internal static CertificateID GenerateCertificateId(X509Certificate issuerCert, BigInteger serialNumber, String hashAlgorithm)
            {
                return new CertificateID(hashAlgorithm, issuerCert, serialNumber);
            }

            internal static Stream GetHttpResponseForOcspRequest(byte[] request, Uri urlt)
            {
                HttpWebRequest con = (HttpWebRequest)WebRequest.Create(urlt);
#if !NETSTANDARD1_6
                con.ContentLength = request.Length;
#endif
                con.ContentType = "application/ocsp-request";
                con.Accept = "application/ocsp-response";
                con.Method = "POST";
                Stream outp = con.GetRequestStream();
                outp.Write(request, 0, request.Length);
                outp.Dispose();
                HttpWebResponse response = (HttpWebResponse)con.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new PdfException(PdfException.InvalidHttpResponse1).SetMessageParams(response.StatusCode);

                return response.GetResponseStream();
            }

            internal static OcspReq GenerateOcspRequestWithNonce(CertificateID id)
            {
                OcspReqGenerator gen = new OcspReqGenerator();
                gen.AddRequest(id);

                // create details for nonce extension
                IDictionary extensions = new Hashtable();

                extensions[OcspObjectIdentifiers.PkixOcspNonce] = new X509Extension(false, new DerOctetString(new DerOctetString(PdfEncryption.GenerateNewDocumentId()).GetEncoded()));

                gen.SetRequestExtensions(new X509Extensions(extensions));
                return gen.Generate();
            }

            internal static bool IsSignatureValid(BasicOcspResp validator, X509Certificate certStoreX509)
            {
                return validator.Verify(certStoreX509.GetPublicKey());
            }

            internal static void IsSignatureValid(TimeStampToken validator, X509Certificate certStoreX509)
            {
                validator.Validate(certStoreX509);
            }

            internal static bool CheckIfIssuersMatch(CertificateID certID, X509Certificate issuerCert)
            {
                return certID.MatchesIssuer(issuerCert);
            }

            internal static DateTime Add180Sec(DateTime date)
            {
                return date.AddSeconds(180);
            }

            internal static IEnumerable<X509Certificate> GetCertsFromOcspResponse(BasicOcspResp ocspResp)
            {
                return ocspResp.GetCerts();
            }

            internal static List<X509Certificate> ReadAllCerts(byte[] contentsKey)
            {
                X509CertificateParser cf = new X509CertificateParser();
                List<X509Certificate> certs = new List<X509Certificate>();

                foreach (X509Certificate cc in cf.ReadCertificates(contentsKey))
                {
                    certs.Add(cc);
                }
                return certs;
            }

            internal static T GetFirstElement<T>(IEnumerable<T> enumerable)
            {
                return enumerable.First();
            }

            internal static X509Name GetIssuerX509Name(Asn1Sequence issuerAndSerialNumber)
            {
                return X509Name.GetInstance(issuerAndSerialNumber[0]);
            }

            internal static String DateToString(DateTime signDate)
            {
                return signDate.ToLocalTime().ToString("yyyy.MM.dd HH:mm:ss zzz");
            }

            internal class TsaResponse
            {
                internal String encoding;
                internal Stream tsaResponseStream;
            }

            internal static TsaResponse GetTsaResponseForUserRequest(String tsaUrl, byte[] requestBytes, String tsaUsername, String tsaPassword)
            {
                HttpWebRequest con;
                try
                {
                    con = (HttpWebRequest)WebRequest.Create(tsaUrl);
                }
                catch (Exception e)
                {
                    throw new PdfException(PdfException.FailedToGetTsaResponseFrom1).SetMessageParams(tsaUrl);
                }
#if !NETSTANDARD1_6
                con.ContentLength = requestBytes.Length;
#endif
                con.ContentType = "application/timestamp-query";
                con.Method = "POST";
                if ((tsaUsername != null) && !tsaUsername.Equals(""))
                {
                    string authInfo = tsaUsername + ":" + tsaPassword;
                    authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
                    con.Headers["Authorization"] = "Basic " + authInfo;
                }
                Stream outp = con.GetRequestStream();
                outp.Write(requestBytes, 0, requestBytes.Length);
                outp.Dispose();
                HttpWebResponse httpWebResponse = (HttpWebResponse)con.GetResponse();

                TsaResponse response = new TsaResponse();
                response.tsaResponseStream = httpWebResponse.GetResponseStream();
                response.encoding = httpWebResponse.Headers[HttpResponseHeader.ContentEncoding];
                return response;
            }

            internal static IEnumerable CreateSigPolicyQualifiers(params SigPolicyQualifierInfo[] sigPolicyQualifierInfo)
            {
                return sigPolicyQualifierInfo;
            }

            internal static DateTime GetTimeStampDate(TimeStampToken timeStampToken)
            {
                return timeStampToken.TimeStampInfo.GenTime;
            }

            internal static ISigner GetSignatureHelper(String algorithm)
            {
                return SignerUtilities.GetSigner(algorithm);
            }

            internal static bool VerifyCertificateSignature(X509Certificate certificate, AsymmetricKeyParameter issuerPublicKey)
            {
                bool res = false;
                try
                {
                    certificate.Verify(issuerPublicKey);
                    res = true;
                }
                catch (Exception ignored)
                {
                }
                return res;
            }

            internal static IEnumerable<X509Certificate> GetCertificates(List<X509Certificate> rootStore)
            {
                return rootStore;
            }
        }
    }
}
