﻿namespace ChaveMovelDigital_NetFramework.Assinatura.Helpers
{
    using iText.IO.Image;
    using iText.Kernel.Geom;
    using Org.BouncyCastle.Security;
    using Org.BouncyCastle.X509;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using DotNetCrypto = System.Security.Cryptography.X509Certificates;

    /// <summary>
    /// Defines the <see cref="CMDAssinaturaOptions" />.
    /// </summary>
    public class CMDAssinaturaOptions
    {
        private readonly IEnumerable<CMDAssinaturaCommitments> __excludedCommitments = new List<CMDAssinaturaCommitments>() { CMDAssinaturaCommitments.None };

        /// <summary>
        /// Defines the __commitments.
        /// </summary>
        private readonly List<CMDAssinaturaCommitments> __commitments = new List<CMDAssinaturaCommitments>();

        /// <summary>
        /// Gets or sets the Appearance
        /// The visual appearance of the signature.....
        /// </summary>
        public AppearanceOptions Appearance { get; set; } = new AppearanceOptions();

        /// <summary>
        /// Gets or sets a value indicating whether AppendTimestampSignature
        /// This field determines whether an additional signature from the timestamp service should be added to the end of the signature list. This is tipically used when the document won't be signed again..
        /// </summary>
        public bool AppendTimestampSignature { get; set; }

        /// <summary>
        /// Gets the Chain
        /// Gets or sets the Chain.....
        /// </summary>
        public IEnumerable<X509Certificate> Chain { get; private set; }

        /// <summary>
        /// Gets the Commitments.
        /// </summary>
        public IReadOnlyCollection<CMDAssinaturaCommitments> Commitments => new ReadOnlyCollection<CMDAssinaturaCommitments>(__commitments.Except(__excludedCommitments).ToList());

        /// <summary>
        /// Gets or sets the Location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the LocationCaption.
        /// </summary>
        public string LocationCaption { get; set; }

        /// <summary>
        /// Gets the Name
        /// Gets or sets the signer's name....
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets or sets the Reason.
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets the ReasonCaption.
        /// </summary>
        public string ReasonCaption { get; set; }

        /// <summary>
        /// Gets or sets the signature field name......
        /// </summary>
        public string SignatureFieldName { get; set; }

        /// <summary>
        /// Gets the SigningCertificate.
        /// </summary>
        public X509Certificate SigningCertificate => Chain?.FirstOrDefault();
        
        /// <summary>
        /// The AddCommitment.
        /// </summary>
        /// <param name="commitment">The commitment<see cref="CMDAssinaturaCommitments"/>.</param>
        /// <returns>The <see cref="CMDAssinaturaOptions"/>.</returns>
        public CMDAssinaturaOptions AddCommitment(CMDAssinaturaCommitments commitment) => AddCommitment(new List<CMDAssinaturaCommitments>() { commitment });
        
        /// <summary>
        /// The AddCommitment
        /// </summary>
        /// <param name="commitments"></param>
        /// <returns></returns>
        public CMDAssinaturaOptions AddCommitment(IEnumerable<CMDAssinaturaCommitments> commitments)
        {
            __commitments.AddRange(commitments.Except(__excludedCommitments).Except(__commitments));

            return this;
        }

        /// <summary>
        /// The ReadChainFromString.
        /// </summary>
        /// <param name="pem">The pem<see cref="string"/>.</param>
        /// <returns>The <see cref="CMDAssinaturaOptions"/>.</returns>
        public CMDAssinaturaOptions ReadChainFromString(string pem)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(pem))
                {
                    throw new ArgumentNullException(nameof(pem));
                }

                Chain = CMDAssinaturaHelper.Crypto.ReadChainFromPemBouncyCastle(pem);

                var c = DotNetUtilities.ToX509Certificate(SigningCertificate);
                var c2 = new DotNetCrypto.X509Certificate2(c);

                Name = c2.GetNameInfo(DotNetCrypto.X509NameType.SimpleName, false);

                return this;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// The RemoveCommitment.
        /// </summary>
        /// <param name="commitment">The commitment<see cref="CMDAssinaturaCommitments"/>.</param>
        /// <returns>The <see cref="CMDAssinaturaOptions"/>.</returns>
        public CMDAssinaturaOptions RemoveCommitment(CMDAssinaturaCommitments commitment) => RemoveCommitment(new List<CMDAssinaturaCommitments>() { commitment });

        /// <summary>
        /// The RemoveCommitment
        /// </summary>
        /// <param name="commitments"></param>
        /// <returns></returns>
        public CMDAssinaturaOptions RemoveCommitment(IEnumerable<CMDAssinaturaCommitments> commitments)
        {
            foreach (var commitment in commitments.Except(__excludedCommitments).Intersect(__commitments))
            {
                __commitments.Remove(commitment);
            }

            return this;
        }
        
        /// <summary>
        /// Defines the <see cref="AppearanceOptions" />.
        /// </summary>
        public class AppearanceOptions
        {
            /// <summary>
            /// Gets or sets the Font
            /// The font to be used on the signature field. Works with font name and font file path.
            /// </summary>
            public string Font { get; set; }

            /// <summary>
            /// Gets or sets the PageNumber
            /// The page number....
            /// </summary>
            public int PageNumber { get; set; } = 1;

            /// <summary>
            /// Gets or sets the SignatureFieldRectangle.
            /// </summary>
            public Rectangle SignatureFieldRectangle { get; set; }

            /// <summary>
            /// Gets or sets the SignatureFieldTextMargins.
            /// </summary>
            public Margins SignatureFieldTextMargins { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether Visible.
            /// </summary>
            public bool Visible { get; set; }

            /// <summary>
            /// Gets or sets the BackgroundImage
            /// The background image.....
            /// </summary>
            public ImageData WatermarkImage { get; set; }
            /// <summary>
            /// Gets or sets the WatermarkMargins.
            /// </summary>
            public Margins WatermarkMargins { get; set; }

            /// <summary>
            /// The SetBackgroundImageFromPath.
            /// </summary>
            /// <param name="path">The path<see cref="string"/>.</param>
            /// <returns>The <see cref="AppearanceOptions"/>.</returns>
            public AppearanceOptions SetBackgroundImageFromPath(string path)
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(path))
                    {
                        throw new ArgumentNullException(nameof(path));
                    }

                    if (!File.Exists(path))
                    {
                        throw new ArgumentException($"Couldn't file the image file ({path})", nameof(path));
                    }

                    var bytes = File.ReadAllBytes(path);

                    if (System.IO.Path.GetExtension(path) == ".png")
                    {
                        return InternalSetBackgroundImage(ImageDataFactory.CreatePng(bytes));
                    }

                    return InternalSetBackgroundImage(ImageDataFactory.CreateJpeg(bytes));
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            /// <summary>
            /// The SetBackgroundImageFromPathJpeg.
            /// </summary>
            /// <param name="path">The path<see cref="string"/>.</param>
            /// <returns>The <see cref="AppearanceOptions"/>.</returns>
            public AppearanceOptions SetBackgroundImageFromPathJpeg(string path)
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(path))
                    {
                        throw new ArgumentNullException(nameof(path));
                    }

                    if (!File.Exists(path))
                    {
                        throw new ArgumentException($"Couldn't file the image file ({path})", nameof(path));
                    }

                    var bytes = File.ReadAllBytes(path);

                    return InternalSetBackgroundImage(ImageDataFactory.CreateJpeg(bytes));
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            /// <summary>
            /// The SetBackgroundImageFromPathPng.
            /// </summary>
            /// <param name="path">The path<see cref="string"/>.</param>
            /// <returns>The <see cref="AppearanceOptions"/>.</returns>
            public AppearanceOptions SetBackgroundImageFromPathPng(string path)
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(path))
                    {
                        throw new ArgumentNullException(nameof(path));
                    }

                    if (!File.Exists(path))
                    {
                        throw new ArgumentException($"Couldn't file the image file ({path})", nameof(path));
                    }

                    var bytes = File.ReadAllBytes(path);

                    return InternalSetBackgroundImage(ImageDataFactory.CreatePng(bytes));
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            /// <summary>
            /// The SetBackgroundImageFromUriJpeg.
            /// </summary>
            /// <param name="uri">The uri<see cref="string"/>.</param>
            /// <returns>The <see cref="AppearanceOptions"/>.</returns>
            public AppearanceOptions SetBackgroundImageFromUriJpeg(string uri)
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(uri))
                    {
                        throw new ArgumentNullException(nameof(uri));
                    }

                    return InternalSetBackgroundImage(ImageDataFactory.CreateJpeg(new Uri(uri)));
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            /// <summary>
            /// The SetBackgroundImageFromUriPng.
            /// </summary>
            /// <param name="uri">The uri<see cref="string"/>.</param>
            /// <returns>The <see cref="AppearanceOptions"/>.</returns>
            public AppearanceOptions SetBackgroundImageFromUriPng(string uri)
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(uri))
                    {
                        throw new ArgumentNullException(nameof(uri));
                    }

                    return InternalSetBackgroundImage(ImageDataFactory.CreatePng(new Uri(uri)));
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            /// <summary>
            /// The SetBackgroundImageJpeg.
            /// </summary>
            /// <param name="image">The image<see cref="byte[]"/>.</param>
            /// <returns>The <see cref="AppearanceOptions"/>.</returns>
            public AppearanceOptions SetBackgroundImageJpeg(byte[] image)
            {
                try
                {
                    if (image == null)
                    {
                        throw new ArgumentNullException(nameof(image));
                    }

                    return InternalSetBackgroundImage(ImageDataFactory.Create(image));
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            /// <summary>
            /// The SetBackgroundImageJpeg.
            /// </summary>
            /// <param name="uri">The uri<see cref="Uri"/>.</param>
            /// <returns>The <see cref="AppearanceOptions"/>.</returns>
            public AppearanceOptions SetBackgroundImageJpeg(Uri uri)
            {
                try
                {
                    if (uri == null)
                    {
                        throw new ArgumentNullException(nameof(uri));
                    }

                    return InternalSetBackgroundImage(ImageDataFactory.CreateJpeg(uri));
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            /// <summary>
            /// The SetBackgroundImagePng.
            /// </summary>
            /// <param name="image">The image<see cref="byte[]"/>.</param>
            /// <returns>The <see cref="AppearanceOptions"/>.</returns>
            public AppearanceOptions SetBackgroundImagePng(byte[] image)
            {
                try
                {
                    if (image == null)
                    {
                        throw new ArgumentNullException(nameof(image));
                    }

                    return InternalSetBackgroundImage(ImageDataFactory.CreatePng(image));
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
            /// <summary>
            /// The SetBackgroundImagePng.
            /// </summary>
            /// <param name="uri">The uri<see cref="Uri"/>.</param>
            /// <returns>The <see cref="AppearanceOptions"/>.</returns>
            public AppearanceOptions SetBackgroundImagePng(Uri uri)
            {
                try
                {
                    if (uri == null)
                    {
                        throw new ArgumentNullException(nameof(uri));
                    }

                    return InternalSetBackgroundImage(ImageDataFactory.CreatePng(uri));
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
            /// <summary>
            /// The InternalSetBackgroundImage.
            /// </summary>
            /// <param name="image">The image<see cref="ImageData"/>.</param>
            /// <returns>The <see cref="AppearanceOptions"/>.</returns>
            private AppearanceOptions InternalSetBackgroundImage(ImageData image)
            {
                try
                {
                    this.WatermarkImage = image;

                    return this;
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            /// <summary>
            /// Defines the <see cref="Margins" />.
            /// </summary>
            public class Margins
            {
                /// <summary>
                /// Gets or sets the Bottom.
                /// </summary>
                public float Bottom { get; set; } = 0;

                /// <summary>
                /// Gets or sets the Left.
                /// </summary>
                public float Left { get; set; } = 0;

                /// <summary>
                /// Gets or sets the Right.
                /// </summary>
                public float Right { get; set; } = 0;

                /// <summary>
                /// Gets or sets the Top.
                /// </summary>
                public float Top { get; set; } = 0;
            }
        }
    }
}
