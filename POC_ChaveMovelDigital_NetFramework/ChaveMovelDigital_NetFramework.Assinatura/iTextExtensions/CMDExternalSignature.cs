﻿using iText.Kernel.Pdf;
using iText.Signatures;
using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaveMovelDigital_NetFramework.Assinatura.iTextExtensions
{
    public class CMDExternalSignature : IExternalSignature
    {
        private readonly byte[] __signature;

        public virtual string HashAlgorithm => DigestAlgorithms.SHA256;

        public CMDExternalSignature(byte[] signature)
        {
            __signature = signature;
        }

        public virtual String GetHashAlgorithm()
        {
            return HashAlgorithm;
        }

        public virtual String GetEncryptionAlgorithm()
        {
            return "RSA";
        }

        public byte[] Sign(byte[] message)
        {
            return __signature;
        }
    }
}
