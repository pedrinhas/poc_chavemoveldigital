﻿namespace ChaveMovelDigital_NetFramework.Assinatura.iTextExtensions
{
    using iText.Forms.Xfdf;
    using iText.Kernel.XMP.Impl;
    using iText.Signatures;
    using Newtonsoft.Json;
    using Org.BouncyCastle.Asn1;
    using Org.BouncyCastle.Asn1.Ocsp;
    using Org.BouncyCastle.Crypto;
    using Org.BouncyCastle.Math;
    using Org.BouncyCastle.Security;
    using Org.BouncyCastle.Tsp;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="TSAMultiClient" />.
    /// </summary>
    public class TSAMultiClient : ITSAClient
    {
        /// <summary>
        /// Defines the DEFAULT_HASH_ALGORITHM.
        /// </summary>
        public const string DEFAULT_HASH_ALGORITHM = "SHA256";

        /// <summary>
        /// Defines the DEFAULT_TOKEN_SIZE.
        /// </summary>
        public const int DEFAULT_TOKEN_SIZE = 4096;

        /// <summary>
        /// Gets the HashAlgorithm
        /// Defines the __hashAlgorithm....
        /// </summary>
        public string HashAlgorithm { get; }

        /// <summary>
        /// Gets the HashAlgorithmOid.
        /// </summary>
        public string HashAlgorithmOid => CryptoConfig.MapNameToOID(HashAlgorithm);

        /// <summary>
        /// Gets the Servers
        /// Gets or sets the Servers....
        /// </summary>
        public List<TSAMultiClientServerInfo> Servers { get; }

        /// <summary>
        /// Gets or sets the Username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the Password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets the TokenSizeEstimate
        /// Defines the __tokenSizeEstimate....
        /// </summary>
        public int TokenSizeEstimate { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TSAMultiClient"/> class.
        /// </summary>
        /// <param name="servers">The servers<see cref="IEnumerable{TSAMultiClientServerInfo}"/>.</param>
        public TSAMultiClient(IEnumerable<TSAMultiClientServerInfo> servers) : this(servers, DEFAULT_HASH_ALGORITHM)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TSAMultiClient"/> class.
        /// </summary>
        /// <param name="servers">The servers<see cref="IEnumerable{TSAMultiClientServerInfo}"/>.</param>
        /// <param name="hashAlgorithm">The hashAlgorithm<see cref="string"/>.</param>
        public TSAMultiClient(IEnumerable<TSAMultiClientServerInfo> servers, string hashAlgorithm) : this(servers, hashAlgorithm, DEFAULT_TOKEN_SIZE)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TSAMultiClient"/> class.
        /// </summary>
        /// <param name="servers">The servers<see cref="IEnumerable{TSAMultiClientServerInfo}"/>.</param>
        /// <param name="estimatedTokenSize">The estimatedTokenSize<see cref="int"/>.</param>
        public TSAMultiClient(IEnumerable<TSAMultiClientServerInfo> servers, int estimatedTokenSize) : this(servers, DEFAULT_HASH_ALGORITHM, estimatedTokenSize)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TSAMultiClient"/> class.
        /// </summary>
        /// <param name="servers">The servers<see cref="IEnumerable{TSAMultiClientServerInfo}"/>.</param>
        /// <param name="hashAlgorithm">The hashAlgorithm<see cref="string"/>.</param>
        /// <param name="estimatedTokenSize">The estimatedTokenSize<see cref="int"/>.</param>
        public TSAMultiClient(IEnumerable<TSAMultiClientServerInfo> servers, string hashAlgorithm, int estimatedTokenSize)
        {
            if (string.IsNullOrWhiteSpace(hashAlgorithm))
            {
                hashAlgorithm = DEFAULT_HASH_ALGORITHM;
            }
            HashAlgorithm = hashAlgorithm;

            if (estimatedTokenSize <= 0)
            {
                estimatedTokenSize = DEFAULT_TOKEN_SIZE;
            }
            TokenSizeEstimate = estimatedTokenSize;

            if (servers == null)
            {
                throw new ArgumentNullException(nameof(servers), $"Server list cannot be null to create {nameof(TSAMultiClient)} instance.");
            }

            var tempValidServers = servers.Where(x => !string.IsNullOrWhiteSpace(x.URL));
            if (!tempValidServers.Any())
            {
                throw new ArgumentNullException(nameof(servers), $"At least one valid server (valid {nameof(TSAMultiClientServerInfo.URL)}) has to be provided for the {nameof(TSAMultiClient)} instance.");
            }


            this.Servers = new List<TSAMultiClientServerInfo>(tempValidServers);
        }

        /// <summary>
        /// The GetMessageDigest.
        /// </summary>
        /// <returns>The <see cref="IDigest"/>.</returns>
        public IDigest GetMessageDigest()
        {
            return DigestUtilities.GetDigest(HashAlgorithm);
        }

        /// <summary>
        /// The GetTimeStampToken.
        /// </summary>
        /// <param name="imprint">The imprint<see cref="byte[]"/>.</param>
        /// <returns>The <see cref="byte[]"/>.</returns>
        public byte[] GetTimeStampToken(byte[] imprint)
        {
            try
            {
                byte[] res = null;

                var generator = new TimeStampRequestGenerator();
                generator.SetCertReq(true);

                var nonce = GenerateNonce();
                var request = generator.Generate(new DerObjectIdentifier(HashAlgorithmOid), imprint, nonce);

                for (int i = 0; i < Servers.Count && res == null; i++)
                {
                    var server = Servers.ElementAt(i);

                    res = GetTSAResponseAsync(server, request).Result;
                }

                if (res == null)
                {
                    throw new CryptographicUnexpectedOperationException("[UTAD] Failed to apply timestamp");
                }

                var response = new TimeStampResponse(res);

                response.Validate(request);

                var failure = response.GetFailInfo();
                if (failure != null && failure.IntValue != 0)
                {
                    throw new InvalidOperationException($"[UTAD] Couldn't get timestamp: error code {failure.IntValue}");
                }

                var encoded = response.TimeStampToken.GetEncoded();

                this.TokenSizeEstimate = encoded.Length + 32;

                return encoded;
            }
            catch (Exception ex)
            {
                //TODO: DEFINITELY log this
                return null;
            }
        }

        /// <summary>
        /// The GetTokenSizeEstimate.
        /// </summary>
        /// <returns>The <see cref="int"/>.</returns>
        public int GetTokenSizeEstimate()
        {
            return TokenSizeEstimate;
        }

        /// <summary>
        /// The GenerateNonce.
        /// </summary>
        /// <returns>The <see cref="BigInteger"/>.</returns>
        protected static BigInteger GenerateNonce()
        {
            var Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            var totalMs = (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;

            var nonce = BigInteger.ValueOf(totalMs);

            return nonce;
        }

        /// <summary>
        /// The GetTSAResponse.
        /// </summary>
        /// <param name="tsaServerInfo">The tsaServerInfo<see cref="TSAMultiClientServerInfo"/>.</param>
        /// <param name="request">The request<see cref="TimeStampRequest"/>.</param>
        /// <returns>The <see cref="byte[]"/>.</returns>
        protected static async Task<byte[]> GetTSAResponseAsync(TSAMultiClientServerInfo tsaServerInfo, TimeStampRequest request)
        {
            try
            {
                if (!tsaServerInfo.AllOk())
                {
                    return null;
                }

                byte[] tsa = null;

                var req = (HttpWebRequest)WebRequest.Create(tsaServerInfo.URL);

                req.Method = "POST";
                req.ContentType = "application/timestamp-query";
                req.Headers.Add("Content-Transfer-Encoding", "binary");

                ApplyAuthenticationToRequest(req, tsaServerInfo);

                var encodedTsaRequest = request.GetEncoded();

                req.ContentLength = encodedTsaRequest.Length;

                using (var requestStream = await req.GetRequestStreamAsync())
                {
                    requestStream.Write(encodedTsaRequest, 0, encodedTsaRequest.Length);
                }

                using (var res = (HttpWebResponse)await req.GetResponseAsync())
                using (var responseStream = res.GetResponseStream())
                using (var ms = new MemoryStream())
                {
                    responseStream.CopyTo(ms);
                    tsa = ms.ToArray();

                    if (res.ContentEncoding.ToLowerInvariant() == "base64")
                    {
                        tsa = Base64.Decode(tsa);
                    }
                }

                return tsa;
            }
            catch (Exception ex)
            {
                //TODO: DEFINITELY log this
                return null;
            }
        }

        protected static void ApplyAuthenticationToRequest(HttpWebRequest req, TSAMultiClientServerInfo tsaServerInfo)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(tsaServerInfo.Username) && !string.IsNullOrWhiteSpace(tsaServerInfo.Password))
                {
                    var b64 = System.Text.Encoding.UTF8.GetBytes($"{tsaServerInfo.Username}:{tsaServerInfo.Password}");
                    var b64string = Convert.ToBase64String(b64, Base64FormattingOptions.None);
                    var authHeader = $"Basic {b64string}";

                    req.Headers.Add(HttpRequestHeader.Authorization, authHeader);
                }

                if (tsaServerInfo.ClientCertificate != null)
                {
                    req.ClientCertificates.Add(tsaServerInfo.ClientCertificate);
                }
            }
            catch (Exception ex)
            {
                //TODO: LOG THIS
                throw;
            }
        }

        /// <summary>
        /// Defines the <see cref="TSAMultiClientServerInfo" />.
        /// </summary>
        public class TSAMultiClientServerInfo : IEquatable<TSAMultiClientServerInfo>
        {
            /// <summary>
            /// Gets the Password.
            /// </summary>
            public string Password { get; }

            /// <summary>
            /// Gets the URL.
            /// </summary>
            public string URL { get; }

            /// <summary>
            /// Gets the Username.
            /// </summary>
            public string Username { get; }

            /// <summary>
            /// Gets or sets the ClientCertificate.
            /// </summary>
            public X509Certificate2 ClientCertificate { get; }

            /// <summary>
            /// Initializes a new instance of the <see cref="TSAMultiClientServerInfo"/> class.
            /// </summary>
            /// <param name="url">The url<see cref="string"/>.</param>
            public TSAMultiClientServerInfo(string url) : this(url, null)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="TSAMultiClientServerInfo"/> class.
            /// </summary>
            /// <param name="url">The url<see cref="string"/>.</param>
            /// <param name="certificate">The certificate<see cref="X509Certificate"/>.</param>
            public TSAMultiClientServerInfo(string url, X509Certificate2 certificate) : this(url, null, null, certificate)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="TSAMultiClientServerInfo"/> class.
            /// </summary>
            /// <param name="url">The url<see cref="string"/>.</param>
            /// <param name="username">The username<see cref="string"/>.</param>
            /// <param name="password">The password<see cref="string"/>.</param>
            public TSAMultiClientServerInfo(string url, string username, string password) : this(url, username, password, null)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="TSAMultiClientServerInfo"/> class.
            /// </summary>
            /// <param name="url">The url<see cref="string"/>.</param>
            /// <param name="username">The username<see cref="string"/>.</param>
            /// <param name="password">The password<see cref="string"/>.</param>
            /// <param name="certificate">The certificate<see cref="X509Certificate"/>.</param>
            public TSAMultiClientServerInfo(string url, string username, string password, X509Certificate2 certificate)
            {
                this.URL = url;
                this.Username = username;
                this.Password = password;
                this.ClientCertificate = certificate;
            }

            /// <summary>
            /// Performs checks specific to this server. To be overridden.
            /// </summary>
            /// <returns></returns>
            public virtual bool AllOk()
            {
                return true;
            }

            public override bool Equals(object obj)
            {
                return this.Equals(obj as TSAMultiClientServerInfo);
            }

            public bool Equals(TSAMultiClientServerInfo other)
            {
                if (other == null)
                {
                    return false;
                }

                if ((other.ClientCertificate == null) != (this.ClientCertificate == null))
                {
                    return false;
                }

                return other.ClientCertificate.Equals(this.ClientCertificate) &&
                    other.Username == this.Username &&
                    other.Password == this.Password &&
                    other.URL == this.URL;
            }
        }

        public class TSAMulticertMulticlientServerInfo : TSAMultiClientServerInfo, IEquatable<TSAMulticertMulticlientServerInfo>
        {
            public string BalanceURL { get; set; }

            /// <summary>
            /// From the signutad app lol
            /// </summary>
            private readonly DateTime __expirationDate = new DateTime(2029, 8, 23);

            public TSAMulticertMulticlientServerInfo(string url, string balanceUrl) : this(url, balanceUrl, null)
            {
            }

            public TSAMulticertMulticlientServerInfo(string url, string balanceUrl, X509Certificate2 certificate) : this(url, balanceUrl, null, null, certificate)
            {
            }

            public TSAMulticertMulticlientServerInfo(string url, string balanceUrl, string username, string password) : this(url, balanceUrl, username, password, null)
            {
            }

            public TSAMulticertMulticlientServerInfo(string url, string balanceUrl, string username, string password, X509Certificate2 certificate) : base(url, username, password, certificate)
            {
                this.BalanceURL = balanceUrl;
            }

            public override bool AllOk()
            {
                try
                {
                    if (DateTime.Now.Date > __expirationDate.Date)
                    {
                        return false;
                    }

                    if (!base.AllOk())
                    {
                        return false;
                    }

                    return CheckBalanceAsync().Result;
                }
                catch (Exception ex)
                { 
                    //TODO: log this pls
                    throw;
                }
            }

            private async Task<bool> CheckBalanceAsync()
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(BalanceURL))
                    {
                        ///no url to check balance, return true until further notice (never)
                        return true;
                    }

                    var result = false;

                    var req = (HttpWebRequest)WebRequest.Create(this.BalanceURL);

                    req.Method = "GET";
                    req.ContentType = "application/json";

                    ApplyAuthenticationToRequest(req, this);

                    using (var res = (HttpWebResponse)await req.GetResponseAsync())
                    using (var responseStream = res.GetResponseStream())
                    using (var sr = new StreamReader(responseStream))
                    {
                        var responseText = await sr.ReadToEndAsync();

                        var balance = TsaBalanceResponse.FromJson(responseText);

                        result = balance.AvailableTimestamps > 0;
                    }

                    return result;
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            public override bool Equals(object obj)
            {
                return this.Equals(obj as TSAMulticertMulticlientServerInfo);
            }

            public bool Equals(TSAMulticertMulticlientServerInfo other)
            {
                var asBase = other as TSAMultiClientServerInfo;

                return other.BalanceURL == this.BalanceURL
                    && base.Equals(asBase);
            }

            private class TsaBalanceResponse
            {
                [JsonProperty("initialTimestamps")]
                public int InitialTimestamps { get; set; }

                [JsonProperty("timestamps")]
                public int AvailableTimestamps { get; set; }

                [JsonProperty("creationDate")]
                public string CreationDate { get; set; } //non standard date format (dd-MM-yyyy), until we do something with this value it's better to play safe and deserialize to string

                [JsonProperty("expires")]
                public int Expires { get; set; } //??

                public static TsaBalanceResponse FromJson(string json)
                {
                    try
                    {
                        if (string.IsNullOrWhiteSpace(json))
                        {
                            throw new ArgumentNullException(json, nameof(json));
                        }

                        var deserialized = JsonConvert.DeserializeObject<TsaBalanceResponse>(json);

                        return deserialized;
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
        }
    }
}
