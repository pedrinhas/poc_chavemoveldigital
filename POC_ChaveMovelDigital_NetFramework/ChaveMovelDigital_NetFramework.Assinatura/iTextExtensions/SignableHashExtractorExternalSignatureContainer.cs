﻿using iText.Kernel.Pdf;
using iText.Signatures;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace ChaveMovelDigital_NetFramework.Assinatura.iTextExtensions
{
    public class SignableHashExtractorExternalSignatureContainer : IExternalSignatureContainer
    {
        //TODO: error handling
        private IEnumerable<X509Certificate> __chain;

        public byte[] SignableHash { get; private set; }
        public byte[] DocumentHash { get; private set; }

        public virtual string HashAlgorithm { get; private set; }

        public SignableHashExtractorExternalSignatureContainer(string alg, IEnumerable<X509Certificate> chain)
        {
            HashAlgorithm = alg;

            __chain = chain;
        }

        public void ModifySigningDictionary(PdfDictionary signDic)
        {
        }

        public byte[] Sign(Stream data)
        {
            IDigest messageDigest = DigestUtilities.GetDigest(HashAlgorithm);
            PdfPKCS7 sign = new PdfPKCS7(null, __chain.ToArray(), HashAlgorithm, false);

            var hash = DigestAlgorithms.Digest(data, messageDigest);
            SignableHash = sign.GetAuthenticatedAttributeBytes(hash, PdfSigner.CryptoStandard.CMS, null, null);
            DocumentHash = hash;

            return new byte[0];
        }
    }
}
