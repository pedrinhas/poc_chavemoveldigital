﻿using iText.Kernel.Pdf;
using iText.Signatures;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace ChaveMovelDigital_NetFramework.Assinatura.iTextExtensions
{
    public class SignableHashExtractorExternalSignature : IExternalSignature
    {
        //TODO: error handling

        public byte[] SignableHash { get; private set; }
        public virtual string HashAlgorithm { get; private set; }

        public SignableHashExtractorExternalSignature() : this(DigestAlgorithms.SHA256)
        {

        }
        public SignableHashExtractorExternalSignature(string alg)
        {
            HashAlgorithm = alg;
        }

        public void ModifySigningDictionary(PdfDictionary signDic)
        {
        }

        public byte[] Sign(byte[] data)
        {
            IDigest messageDigest = DigestUtilities.GetDigest(HashAlgorithm);
            //SignableHash = DigestAlgorithms.Digest(new MemoryStream(data), messageDigest);
            SignableHash = data;

            return new byte[0];
        }

        public string GetHashAlgorithm()
        {
            return HashAlgorithm;
        }

        public string GetEncryptionAlgorithm()
        {
            return "RSA";
        }
    }
}
