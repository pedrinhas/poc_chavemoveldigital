﻿using iText.Kernel.Pdf;
using iText.Signatures;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ChaveMovelDigital_NetFramework.Assinatura.iTextExtensions
{
    public class CMDExternalSignatureContainer : IExternalSignatureContainer
    {
        private readonly byte[] __signature;
        private readonly byte[] __documentHash;
        private readonly IEnumerable<X509Certificate> __chain;
        private readonly ITSAClient __tsaClient;
        private readonly ICollection<byte[]> __ocsp;
        private readonly ICollection<byte[]> __crlBytes;

        public virtual string HashAlgorithm => DigestAlgorithms.SHA256;

        public CMDExternalSignatureContainer(byte[] signature, IEnumerable<X509Certificate> certificateChain) : this(signature, null, certificateChain, default, default, default)
        {
        }

        public CMDExternalSignatureContainer(byte[] signature, byte[] documentHash, IEnumerable<X509Certificate> certificateChain) : this(signature, documentHash, certificateChain, default, default, default)
        {
        }

        public CMDExternalSignatureContainer(byte[] signature, IEnumerable<X509Certificate> certificateChain, ITSAClient tsaClient, ICollection<byte[]> ocsp, ICollection<byte[]> crlBytes) : this(signature, null, certificateChain, tsaClient, ocsp, crlBytes)
        {

        }

        public CMDExternalSignatureContainer(byte[] signature, byte[] documentHash, IEnumerable<X509Certificate> certificateChain, ITSAClient tsaClient, ICollection<byte[]> ocsp, ICollection<byte[]> crlBytes)
        {
            __signature = signature;
            __documentHash = documentHash;
            __chain = certificateChain;

            __tsaClient = tsaClient;
            __ocsp = ocsp;
            __crlBytes = crlBytes;
        }

        public void ModifySigningDictionary(PdfDictionary signDic)
        {
        }

        public byte[] Sign(Stream data)
        {
            var hash = __documentHash;

            if (hash == null || !hash.Any())
            {
                IDigest messageDigest = DigestUtilities.GetDigest(HashAlgorithm);
                hash = DigestAlgorithms.Digest(data, messageDigest);
            }

            PdfPKCS7 sign = new PdfPKCS7(null, __chain.ToArray(), HashAlgorithm, false);

            sign.SetExternalDigest(__signature, null, GetEncryptionAlgorithm());

            return sign.GetEncodedPKCS7(hash, PdfSigner.CryptoStandard.CMS, __tsaClient, __ocsp, __crlBytes);
        }

        public virtual String GetHashAlgorithm()
        {
            return DigestAlgorithms.SHA256;
        }

        public virtual String GetEncryptionAlgorithm()
        {
            return "RSA";
        }
    }
}
