﻿using ChaveMovelDigital_NetFramework.Assinatura.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaveMovelDigital_NetFramework.Tests
{
    [TestClass]
    public class HelperTests : BaseTests
    {
        [TestClass]
        public class CryptoTests : HelperTests
        {
            private const string someString = "hello world";

            [TestMethod]
            public void EncryptBytes()
            {
                var encrypted = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, someString);

                Assert.IsNotNull(encrypted);
                Assert.IsTrue(encrypted.Any());
            }

            [TestMethod]
            public void GetPdfHash()
            {
                var fileBytes = File.ReadAllBytes(__dummy_pdf);

                var signableHash = CMDAssinaturaHelper.PDF.PreSignDocument(fileBytes, new CMDAssinaturaOptions());
            }
        }
    }
}
