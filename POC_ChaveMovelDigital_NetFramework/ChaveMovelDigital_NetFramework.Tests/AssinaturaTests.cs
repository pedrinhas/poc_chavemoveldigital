﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using ChaveMovelDigital_NetFramework.Assinatura.Helpers;
using ChaveMovelDigital_NetFramework.Assinatura.iTextExtensions;
using ChaveMovelDigital_NetFramework.Tests.Services.CMDAssinatura;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.OpenSsl;

namespace ChaveMovelDigital_NetFramework.Tests
{
    [TestClass]
    public class AssinaturaTests : BaseTests
    {
        protected byte[] __oid => CryptoConfig.EncodeOID(CryptoConfig.MapNameToOID(nameof(SHA256)));

        protected SCMDServiceClient GetAssertedSCMDServiceClient()
        {
            var client = new SCMDServiceClient();

            Assert.IsNotNull(client);
            //Assert.AreNotEqual(System.ServiceModel.CommunicationState.Faulted, client.State);

            return client;
        }

        [TestMethod]
        public async Task SignSomeDocumentLocalCert()
        {
            try
            {
                var localCert = @"..\..\Local Cert\new\cert_and_key.pem";
                var localKey = @"..\..\Local Cert\privkey_nopwd.pem";

                var options = new CMDAssinaturaOptions()
                {
                    LocationCaption = "Departamento",
                    ReasonCaption = "Despacho",
                    Location = "",
                    Reason = "Recusado. Esta razão é muito complexa por isso precisa de muito texto. Este texto é longo porque a razão é complexa.",
                    SignatureFieldName = "some_field",
                    AppendTimestampSignature = true,
                    Appearance = new CMDAssinaturaOptions.AppearanceOptions()
                    {
                        Visible = true,
                        Font = @"c:\Windows\Fonts\tahoma.ttf",
                        SignatureFieldRectangle = new iText.Kernel.Geom.Rectangle(100, 100, 170, 90),
                        SignatureFieldTextMargins = new CMDAssinaturaOptions.AppearanceOptions.Margins()
                        {
                            Top = 5,
                            Bottom = 5,
                            Left = 10,
                            Right = 10
                        },
                        WatermarkMargins = new CMDAssinaturaOptions.AppearanceOptions.Margins()
                        {
                            Top = 2,
                            Bottom = 2,
                            Left = 5,
                            Right = 5
                        }
                    }.SetBackgroundImageFromPath(__logo_utad_watermark)
                };

                options.AddCommitment(new List<CMDAssinaturaCommitments>() 
                { 
                    CMDAssinaturaCommitments.ProofOfApproval, 
                    CMDAssinaturaCommitments.DataAuthentication, 
                    CMDAssinaturaCommitments.BoundToDataSigned,
                    //this is ignored
                    CMDAssinaturaCommitments.None
                });

                var certs = File.ReadAllText(localCert);

                options.ReadChainFromString(certs);

                var fileBytes = File.ReadAllBytes(__dummy_pdf_local_cert);
                var fileBytesWithSigField = CMDAssinaturaHelper.PDF.AddSignatureField(fileBytes, options);

                var preSignResult = CMDAssinaturaHelper.PDF.PreSignDocument(fileBytesWithSigField, options);

                AsymmetricCipherKeyPair pair;
                using (var reader = File.OpenText(localKey))
                {
                    pair = (AsymmetricCipherKeyPair)new PemReader(reader).ReadObject();
                }

                var encryptEngine = new Pkcs1Encoding(new RsaEngine());
                encryptEngine.Init(true, pair.Private);

                var signed = encryptEngine.ProcessBlock(preSignResult.SignableHashDigestInfo, 0, preSignResult.SignableHashDigestInfo.Length);

                var cert = new X509Certificate2(__tsa_ano_cert_store, __tsa_ano_cert_store_password);

                _add_tsa_servers();

                var signedPdf = CMDAssinaturaHelper.PDF.AppendSignature(preSignResult, signed, options);

                File.WriteAllBytes(__dummy_pdf_local_cert_signed, signedPdf);

                System.Diagnostics.Process.Start(__dummy_pdf_local_cert_signed);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [TestMethod]
        public void SetDefaultTSAServers()
        {
            var cert = new X509Certificate2(__tsa_ano_cert_store, __tsa_ano_cert_store_password);

            Assert.IsNotNull(cert);

            CMDAssinaturaHelper.PDF.AddDefaultTSAServer(new TSAMultiClient.TSAMulticertMulticlientServerInfo(__tsa_ano_balance_url, __tsa_ano_timestamp_url, cert));
            Assert.AreEqual(1, CMDAssinaturaHelper.PDF.DefaultTSAServers.Count);

            CMDAssinaturaHelper.PDF.AddDefaultTSAServer(new TSAMultiClient.TSAMulticertMulticlientServerInfo(__tsa_ano_balance_url, __tsa_ano_timestamp_url, cert));
            Assert.AreEqual(1, CMDAssinaturaHelper.PDF.DefaultTSAServers.Count);

            CMDAssinaturaHelper.PDF.AddDefaultTSAServer(new TSAMultiClient.TSAMultiClientServerInfo(__tsa_cc_url));
            Assert.AreEqual(2, CMDAssinaturaHelper.PDF.DefaultTSAServers.Count);
        }

        [TestMethod]
        public async Task AppendTimestamp()
        {
            try
            {
                string pdfSourcePath = __dummy_pdf;
                string pdfDestPath = __dummy_pdf_timestamped;

                var fileBytes = File.ReadAllBytes(pdfSourcePath);

                _add_tsa_servers();

                var signedPdf = CMDAssinaturaHelper.PDF.AppendTimestamp(fileBytes);

                File.WriteAllBytes(pdfDestPath, signedPdf);

                System.Diagnostics.Process.Start(pdfDestPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [TestMethod]
        public async Task SignSomeDocument()
        {
            try
            {
                string pdfSourcePath = __dummy_pdf;
                string pdfDestPath = __dummy_pdf_signed;

                var options = new CMDAssinaturaOptions()
                {
                    LocationCaption = "Departamento",
                    Location = "SIC",
                    ReasonCaption = "Despacho",
                    Reason = "Recusado",
                    SignatureFieldName = "some_field",
                    AppendTimestampSignature = false,
                    Appearance = new CMDAssinaturaOptions.AppearanceOptions()
                    {
                        Visible = true,
                        SignatureFieldRectangle = new iText.Kernel.Geom.Rectangle(0, 0, 100, 50)
                    }.SetBackgroundImageFromPath(__logo_utad_watermark)
                };

                var telEncrypted = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, __tel_claudio);
                var pinEncrypted = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, __pin_claudio);

                var someClient = GetAssertedSCMDServiceClient();

                var certs = await someClient.GetCertificateAsync(__application_id_bytes, telEncrypted);
                Assert.IsNotNull(certs);

                options.ReadChainFromString(certs);
                Assert.IsTrue(options.Chain != null && options.Chain.Any());

                var fileBytes = File.ReadAllBytes(pdfSourcePath);
                var preSignResult = CMDAssinaturaHelper.PDF.PreSignDocument(fileBytes, options);

                var signResponse = await someClient.SCMDSignAsync(new SignRequest()
                {
                    ApplicationId = __application_id_bytes,
                    DocName = "someDoc",
                    Hash = preSignResult.SignableHashDigestInfo,
                    UserId = telEncrypted,
                    Pin = pinEncrypted
                });

                Assert.IsNotNull(signResponse);
                Assert.AreEqual(CMDAssinaturaHelper.StatusCodes.ParseStatusCode(signResponse.Code), CMDAssinaturaHelper.StatusCodes.StatusCode.Ok);

                var otp = "some_pin_breakpoint_and_manually_change";

                var encryptedOtp = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, otp);
                
                var otpResponse = await someClient.ValidateOtpAsync(encryptedOtp, signResponse.ProcessId, __application_id_bytes);
                
                Assert.IsNotNull(otpResponse);
                Assert.AreEqual(CMDAssinaturaHelper.StatusCodes.ParseStatusCode(otpResponse.Status.Code), CMDAssinaturaHelper.StatusCodes.StatusCode.Ok);

                _add_tsa_servers();

                var signedPdf = CMDAssinaturaHelper.PDF.AppendSignature(preSignResult, otpResponse.Signature, options);

                File.WriteAllBytes(pdfDestPath, signedPdf);

                System.Diagnostics.Process.Start(pdfDestPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [TestMethod]
        public async Task GetUserCertificate()
        {
            try
            {
                var someClient = GetAssertedSCMDServiceClient();

                var telEncrypted = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, __tel_claudio);
                var pinEncrypted = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, __pin_claudio);

                var certResponse = await someClient.GetCertificateAsync(__application_id_bytes, telEncrypted);

                Assert.IsNotNull(certResponse);
                Assert.IsFalse(string.IsNullOrWhiteSpace(certResponse));

                var certs = CMDAssinaturaHelper.Crypto.ReadChainFromPemBouncyCastle(certResponse);
                Assert.IsNotNull(certResponse);
                Assert.IsTrue(certs.Any());
                Assert.IsTrue(certs.All(x => x != null));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [TestMethod]
        public async Task BadOTP()
        {
            try
            {
                var someClient = GetAssertedSCMDServiceClient();

                var telEncrypted = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, __tel_claudio);
                var pinEncrypted = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, __pin_claudio);

                var certResponse = await someClient.GetCertificateWithPinAsync(__application_id_bytes, telEncrypted, pinEncrypted);

                Assert.IsNotNull(certResponse);
                Assert.AreEqual(CMDAssinaturaHelper.StatusCodes.ParseStatusCode(certResponse.Code), CMDAssinaturaHelper.StatusCodes.StatusCode.Ok);

                var otp = "654610";

                var encryptedOtp = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, otp);

                var otpResponse = await someClient.ValidateOtpAsync(encryptedOtp, certResponse.ProcessId, __application_id_bytes);

                Assert.IsNotNull(otpResponse);
                Assert.AreEqual(CMDAssinaturaHelper.StatusCodes.ParseStatusCode(otpResponse.Status.Code), CMDAssinaturaHelper.StatusCodes.StatusCode.BadOTP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [TestMethod]
        public async Task BadOTPNonDigit()
        {
            try
            {
                var someClient = GetAssertedSCMDServiceClient();

                var telEncrypted = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, __tel_claudio);
                var pinEncrypted = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, __pin_claudio);

                var certResponse = await someClient.GetCertificateWithPinAsync(__application_id_bytes, telEncrypted, pinEncrypted);

                Assert.IsNotNull(certResponse);
                Assert.AreEqual(CMDAssinaturaHelper.StatusCodes.ParseStatusCode(certResponse.Code), CMDAssinaturaHelper.StatusCodes.StatusCode.Ok);

                var otp = "primaprima";

                var encryptedOtp = CMDAssinaturaHelper.Crypto.EncryptWithCertToBase64(__cert_path, __cert_thumbprint, otp);

                Assert.ThrowsException<TimeoutException>(() =>
                {
                    someClient.ValidateOtp(encryptedOtp, certResponse.ProcessId, __application_id_bytes);
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [TestMethod]
        public void GetCRLResponseFromCertificate()
        {
            try
            {
                var localCert = @"..\..\Local Cert\myCert.cer";

                var options = new CMDAssinaturaOptions();

                var certs = File.ReadAllText(localCert);
                options.ReadChainFromString(certs);

                //ugly using reflection, oh well
                var crlMethod = typeof(CMDAssinaturaHelper.PDF).GetMethod("GetCRLResponse", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
                var crl = crlMethod.Invoke(this, new object[] { options }) as ICollection<byte[]>;

                Assert.IsNotNull(crl);
                Assert.IsTrue(crl.Any());
                Assert.IsTrue(crl.All(x => x != null && x.Any()));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [TestMethod]
        public void GetOCSPResponseFromCertificate()
        {
            try
            {
                var localCert = @"..\..\Local Cert\myCert.cer";

                var options = new CMDAssinaturaOptions();

                var certs = File.ReadAllText(localCert);
                options.ReadChainFromString(certs);

                //ugly using reflection, oh well
                var ocspMethod = typeof(CMDAssinaturaHelper.PDF).GetMethod("GetOCSPResponse", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
                var ocsp = ocspMethod.Invoke(this, new object[] { options }) as ICollection<byte[]>;

                Assert.IsNotNull(ocsp);
                Assert.IsTrue(ocsp.Any());
                Assert.IsTrue(ocsp.All(x => x != null && x.Any()));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void _add_tsa_servers()
        {
            var cert = new X509Certificate2(__tsa_ano_cert_store, __tsa_ano_cert_store_password);

            CMDAssinaturaHelper.PDF.AddDefaultTSAServer(new TSAMultiClient.TSAMultiClientServerInfo(__tsa_cc_url));
            CMDAssinaturaHelper.PDF.AddDefaultTSAServer(new TSAMultiClient.TSAMulticertMulticlientServerInfo(__tsa_ano_balance_url, __tsa_ano_timestamp_url, cert));
        }
    }
}
