﻿using ChaveMovelDigital_NetFramework.Assinatura.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace ChaveMovelDigital_NetFramework.Tests
{
    public class BaseTests
    {
        protected const string __tel_claudio = "+351 939436190";
        protected const string __pin_claudio = "123456";
        protected const string __dummy_pdf = @"..\..\PDF\dummy.pdf";
        protected const string __dummy_pdf_signed = @"..\..\PDF\dummy_signed.pdf";
        protected const string __dummy_pdf_timestamped = @"..\..\PDF\dummy_timestamped.pdf";
        protected const string __dummy_pdf_local_cert = __dummy_pdf;
        protected const string __dummy_pdf_local_cert_signed = @"..\..\PDF\dummy_local_cert_signed.pdf";
        protected const string __dummy_pdf_with_field = @"..\..\PDF\dummy_with_field.pdf";
        protected const string __dummy_pdf_with_field_signed = @"..\..\PDF\dummy_with_field_signed.pdf";
        protected const string __application_id_string = "2101556b-e333-476f-83b5-52845915b31c";

        protected const string __tsa_cc_url = "http://ts.cartaodecidadao.pt/tsa/server";
        protected const string __tsa_ano_cert_store = @"..\..\SSL\store.p12";
        protected const string __tsa_ano_cert_store_password = @"Z7S]F_sfgYxgIYlzGXBvTuZP>3mVc-[l)b(1(_6QaXx{Cdw9hVnN<KJUb>gRb(eB";
        protected const string __tsa_ano_balance_url = "https://tsa.multicert.com/pec/pecsvcq/00000PT501345361E5DYIEIE";
        protected const string __tsa_ano_timestamp_url = "https://tsa.multicert.com/pec/requests/00000PT501345361E5DYIEIE";

        protected const string __cert_thumbprint = "98c97357e068e251bafc107d978e38f1aa39f3ea";
        protected const string __cert_path = @"..\..\Cert\certnew.p7b .spc";

        protected const string __logo_utad_watermark = @"..\..\Image\utad_azul.png";
        protected const string __manuscript_signature_watermark = @"..\..\Image\manuscript_signature.jpeg";

        protected Guid __application_id => Guid.Parse(__application_id_string);
        protected byte[] __application_id_bytes => CMDAssinaturaHelper.Conversions.StringToByteArray(__application_id_string);

        protected Stream GetFileStreamAsserted(string path = __dummy_pdf, FileMode fileMode = FileMode.Open)
        {
            try
            {
                Assert.IsNotNull(path);
                Assert.IsFalse(string.IsNullOrWhiteSpace(path));

                var fs = new FileStream(path, fileMode);

                Assert.IsNotNull(fs);

                return fs;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //TODO: put all of this in a helper class/library
        protected byte[] GetByteArrayHashSHA256(byte[] input)
        {
            return GetByteArrayHash(input, SHA256.Create());
        }

        protected byte[] GetByteArrayHash(byte[] input, HashAlgorithm alg)
        {
            try
            {
                if(input == null)
                {
                    return null;
                }

                var res = alg.ComputeHash(input);

                return res;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected byte[] GetFileHashSHA256(string path)
        {
            return GetFileHash(path, SHA256.Create());
        }

        protected byte[] GetFileHash(string path, HashAlgorithm alg)
        {
            try
            {
                using (var fileStream = GetFileStreamAsserted(path))
                {
                    var fileBytes = new byte[fileStream.Length];

                    fileStream.Read(fileBytes, 0, fileBytes.Length);

                    var hashed = GetByteArrayHash(fileBytes, alg);

                    return hashed;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected string GetFileHashAsString(string path, HashAlgorithm alg)
        {
            try
            {
                var hash = GetFileHash(path, alg);

                var sb = new StringBuilder();

                foreach (byte b in hash)
                    sb.Append(b.ToString("x2"));

                return sb.ToString();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected string GetFileHashSHA256AsString(string path)
        {
            return GetFileHashAsString(path, SHA256.Create());
        }
    }
}
